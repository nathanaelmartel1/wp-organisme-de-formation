jQuery(document).ready(function($)
{
    var datepicker_trois_mois =
    {
        dateFormat: 'dd/mm/yy',
        numberOfMonths: 3,
    };
    
    // Bouton de vérification des custom keywords
    $(".doc-check-ckw").click(doc_check_ckw);
    function doc_check_ckw(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        
        jQuery.post
        ({
            url: ajaxurl,
            async: false,
            data: {
                'action': 'doc_check_custom_keywords',
                'contexte': contexte,
                'contexte_id': contexte_id,
                'session_id': session,
                'type_doc': type_doc,
            },
            success: function(response)
            {
                param = JSON.parse(response);
                dialog_box = $(param.dialog);
                dialog_box.on('click', '.openButton', toggle_bloc_hidden);
                dialog_box.dialog
                ({
                    autoOpen: false,
                    minWidth: 400,
                    width: 'auto',
                    modal: true,
                    title: "Informations à renseigner pour un document complet",
                    buttons:
                    {
                        "Valider": function()
                        {
                            dialog_box = $(this);
                            form = dialog_box.find("form")[0];
                            formData = new FormData(form);
                            dialog_box.find("textarea.wp-editor-area").each(function()
                            {
                                name = $(this).attr('id');
                                formData.set(name, tinyMCE.get(name).getContent());
                                tinyMCE.execCommand("mceRemoveEditor", false, $(this).attr('id'));
                            });
                            jQuery.post
                            ({
                                url: ajaxurl,
                                data: formData,
                                processData: false,
                                contentType: false,
                                success: function (response)
                                {
                                    param = JSON.parse(response);
                                    newline = $(param.lignedoc);
                                    newline.on('click', '.doc-creer', doc_creer);
                                    newline.on('click', '.doc-check-ckw', doc_check_ckw);
                                    newline.on('click', '.infos-manquantes', doc_infos_manquantes);
                                    newline.on('click', '.doc-demande-valid', doc_demande_valid);
                                    newline.on('click', '.doc-diffuser', doc_diffuser);
                                    newline.on('click', '.doc-scan', doc_scan);
                                    newline.on('click', '.doc-supprimer', doc_supprimer);
                                    newline.on('change', '.input_jpost input', change_jpost_value);
                                    newline.on('change', '.input_jpost select', change_jpost_value);
                                    newline.on('focus', '.input_jpost_value.datepicker', function() { $(this).datepicker(datepicker_trois_mois); });
                                    ligne.replaceWith(newline);
                                },
                            });
                            dialog_box.dialog("close");
                        },
                    },
                    close:function()
                    {
                        dialog_box.dialog("destroy");
                    },
                });
                dialog_box.ready(function()
                {
                    dialog_box.find('textarea.wp-editor-area').each(function()
                    {
                        tinyMCE.execCommand("mceAddEditor", false, $(this).attr('id'));
                    });
                    dialog_box.on('click', '.add_keyword_defaut', add_keyword_defaut);
                });
                dialog_box.dialog("open");
            }
        });
    }
    
    // Copier la valeur par défaut dans le champ input correspondant
    $(".add_keyword_defaut").click(add_keyword_defaut);
    function add_keyword_defaut(e)
    {
        e.preventDefault();
        
        input_id = $(this).attr('data-id');
        input_type = $(this).attr('data-type');
        content = $(this).attr('data-content');
        
        switch(input_type)
        {
            case "bool":
                suffix = (content == 1) ? "oui" : "non";
                $("#" + input_id + suffix).prop('checked', true);
                break;
            case "text":
                tinyMCE.get(input_id.replace("ckw_", "")).setContent(content);
                break;
            case "number":
                $("#" + input_id).val(content);
                break;
            default:
                break;
        }
    }
    
    // Bouton créer ou signer le document
    $(".doc-creer").click(doc_creer);
    function doc_creer(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        doc_uid = ligne.attr("data-docuid");
        attention = $(this).hasClass("attention");
        action_doc = "brouillon";
        var ckw = 0;
        if ($(this).attr("data-final"))
            action_doc = "final";
        else
            ckw = $(this).attr('data-ckw');
        cols = ligne.attr("data-cols");
        if ($(this).attr("data-signataire"))
            signataire = 1;
        else
            signataire = 0;
        
        
        upload_form_name = "upload-" + doc_uid;
        
        if (!attention)
        {
            bouton_text = $("#nom-" + doc_uid).html();
            $("#nom-" + doc_uid).html("Patientez");
        
            jQuery.post
            ({
                url: ajaxurl,
                asinc: false,
                data: {
                    'action': 'traitement_doc',
                    'contexte_id': contexte_id,
                    'contexte': contexte,
                    'session_id': session,
                    'type_doc': type_doc,
                    'cols': cols,
                    'signataire': signataire,
                    'action_doc': action_doc,
                },
                success: function(response)
                {
                    param = JSON.parse(response);
                    newline = $(param.lignedoc);
                    newline.on('click', '.doc-creer', doc_creer);
                    newline.on('click', '.doc-check-ckw', doc_check_ckw);
                    newline.on('click', '.infos-manquantes', doc_infos_manquantes);
                    newline.on('click', '.doc-demande-valid', doc_demande_valid);
                    newline.on('click', '.doc-diffuser', doc_diffuser);
                    newline.on('click', '.doc-scan', doc_scan);
                    newline.on('click', '.doc-supprimer', doc_supprimer);
                    newline.on('change', '.input_jpost input', change_jpost_value);
                    newline.on('change', '.input_jpost select', change_jpost_value);
                    newline.on('focus', '.input_jpost_value.datepicker', function() { $(this).datepicker(datepicker_trois_mois); });
                    ligne.replaceWith(newline);
                    if (param.message !== undefined)
                        show_message(param.message);
                }
            });
        }
    }
    
    // Afficher les infos manquantes d'un document dans la message box
    $(".docrow .infos-manquantes").click(doc_infos_manquantes);
    function doc_infos_manquantes(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'doc_get_infos_manquantes',
                'contexte_id': contexte_id,
                'contexte': contexte,
                'session_id': session,
                'type_doc': type_doc,
            },
            function(response)
            {
                $("#opaga-message .message").html("");
                show_message(response);
            },
        );
    }


    // Bouton demander la validation (signature par le responsable de formation)
    $(".doc-demande-valid").click(doc_demande_valid);
    function doc_demande_valid(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        doc_uid = ligne.attr("data-docuid");
        cols = ligne.attr("data-cols");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'traitement_doc',
                'contexte_id': contexte_id,
                'contexte': contexte,
                'session_id': session,
                'type_doc': type_doc,
                'cols': cols,
                'action_doc': 'demande_valid',
            },
            function(response)
            {
                param = JSON.parse(response);
                show_message(param.message);
            }
        );
        
        // plutôt que toggle, vérifier le véritable état !
        $(this).toggleClass("en-cours");
    }

    // Bouton pour diffuser le document au stagiaire
    $(".doc-diffuser").click(doc_diffuser);
    function doc_diffuser(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        doc_uid = ligne.attr("data-docuid");
        cols = ligne.attr("data-cols");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'traitement_doc',
                'contexte_id': contexte_id,
                'contexte': contexte,
                'session_id': session,
                'type_doc': type_doc,
                'cols': cols,
                'action_doc': 'diffuser',
            },
            function(response)
            {
                param = JSON.parse(response);
                show_message(param.message);
            }
        );
        
        // plutôt que toggle, vérifier le véritable état !
        $(this).toggleClass("fait");
    }

    // Bouton pour supprimer le document
    $(".doc-supprimer").click(doc_supprimer);
    function doc_supprimer(e)
    {
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        doc_uid = ligne.attr("data-docuid");
        cols = ligne.attr("data-cols");
        
        var supprim = true;
        yesno = $("#supprim" + doc_uid);
        yesno.dialog(
        {
            autoOpen: true,
            title: "Êtes-vous sûr ?",
            height: 200,
            width: 350,
            modal: true,
            buttons:
            {
                "Oui": function()
                {
                    jQuery.post
                    (
                        ajaxurl,
                        {
                            'action': 'traitement_doc',
                            'contexte_id': contexte_id,
                            'contexte': contexte,
                            'session_id': session,
                            'type_doc': type_doc,
                            'cols': cols,
                            'action_doc': 'supprimer',
                        },
                        function(response)
                        {
                            param = JSON.parse(response);
                            newline = $(param.lignedoc);
                            newline.on('click', '.doc-creer', doc_creer);
                            newline.on('click', '.doc-demande-valid', doc_demande_valid);
                            newline.on('click', '.doc-diffuser', doc_diffuser);
                            newline.on('click', '.doc-scan', doc_scan);
                            newline.on('click', '.doc-supprimer', doc_supprimer);
                            ligne.replaceWith(newline);
                        }
                    );
                    yesno.dialog("destroy");
                },
                "Non": function() { supprim = false; yesno.dialog("destroy");},
            },
            close: function()
            {
                supprim = false; yesno.dialog("destroy");
            }
        });
        yesno.dialog("open");
    }
    
    $('.doc-rejeter').click(function(e)
    {
        e.preventDefault();
        
        var ligne = $(this).closest(".docrow");
        contexte_id = ligne.attr("data-contexteid");
        contexte = ligne.attr("data-contexte");
        session = ligne.attr("data-sessionid");
        type_doc = ligne.attr("data-typedoc");
        cols = ligne.attr("data-cols");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'traitement_doc',
                'contexte_id': contexte_id,
                'contexte': contexte,
                'session_id': session,
                'type_doc': type_doc,
                'cols': cols,
                'action_doc': 'rejeter',
            },
            function(response)
            {
                ligne.remove();
            }
        );
        
        
    });
    

    // Modification du document en HTML
    $(".doc-edit-html").click(function(e)
    {
        e.preventDefault();
        
        info_doc = $(this).closest(".docrow");
        
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'get_html_doc_content',
                'session_id': info_doc.attr('data-sessionid'),
                'contexte': info_doc.attr('data-contexte'),
                'contexte_id': info_doc.attr('data-contexteid'),
                'typedoc': info_doc.attr('data-typedoc'),
            },
            function(response)
            {
                param = JSON.parse(response);
                dialog_box = $(param.html);
                dialog_box.dialog
                ({
                    autoOpen: true,
                    minWidth: 700,
                    width: 'auto',
                    modal: true,
                    buttons:
                    {
                        "Valider": function()
                        {
                            dialog_box.find("textarea.wp-editor-area").each(function()
                            {
                                name = $(this).attr('id');
                                content = tinyMCE.get(name).getContent();
                                console.log(content);
                                jQuery.post
                                (
                                    ajaxurl,
                                    {
                                        'action': 'update_html_doc_content',
                                        'session_id': info_doc.attr('data-sessionid'),
                                        'contexte': info_doc.attr('data-contexte'),
                                        'contexte_id': info_doc.attr('data-contexteid'),
                                        'typedoc': info_doc.attr('data-typedoc'),
                                        'content': content,
                                    },
                                    function(response)
                                    {
                                        
                                    }
                                )
                            });
                            $(this).dialog("close");
                        },
                        "Fermer": function()
                        {
                            $(this).dialog("close");
                        },
                    },
                    close:function()
                    {
                        tinyMCE.execCommand("mceRemoveEditor", false, $(this).attr('id'));
                        $(this).dialog("destroy");
                    }
                });
                dialog_box.ready(function()
                {
                    dialog_box.find('textarea.wp-editor-area').each(function()
                    {
                        tinyMCE.execCommand("mceAddEditor", false, $(this).attr('id'));
                    });
                });
            }
        );
    });
    
    // envoyer un fichier libre en ajax post
    $(".ajax-save-file").click(function(e)
    {
        form = $(this).closest('form');
        save_file(form);
    });

    // fonction qui gère le dépot de fichier, soit prédéfini (input[name='document'] est renseigné), soit libre
    function save_file(form)
    {
        var id_span_message = "#" + form.find("input[name='id_span_message']").val();
        var id_span_filename = "#" + form.find("input[name='id_span_filename']").val();
        var ligne_id = "#" + form.find("input[name='ligne_id']").val();
        var ligne = $(ligne_id);
        
        filelist = form.children("input[type='file']")[0].files;
        json_filelist = Array();
        
        var formData = new FormData();
    //    formData.set('file', $(this).parent().children("input[type='file']").prop('files'));
        for (var i = 0; i < filelist.length; i++)
        {
            formData.append("files[]", (filelist[i]));
        }
        
        formData.set('action', form.find("input[name='action']").val());
        formData.set('session_id', form.find("input[name='session_id']").val());
        formData.set('nb_files', filelist.length);
                        
        if (ligne.attr('data-typedoc') != undefined)
        {
            formData.set('contexte_id', ligne.attr("data-contexteid"));
            formData.set('contexte', ligne.attr("data-contexte"));
            formData.set('signataire', form.find("input[name='signataire']").val());
            formData.set('signature_responsable', form.find("input[name='signature_responsable']").prop('checked'));
            formData.set('signature_client', form.find("input[name='signature_client']").prop('checked'));
            formData.set('signature_stagiaire', form.find("input[name='signature_stagiaire']").prop('checked'));
            formData.set('cols', ligne.attr("data-cols"));
            formData.set('type_doc', ligne.attr("data-typedoc"));
            formData.set('doc_uid', ligne.attr("data-docuid"));
        }
        
        $(id_span_message).html('Patientez');
        
        jQuery.post
        ({
            url: ajaxurl,
            data: formData,
            processData: false,
            contentType: false,
            success: function(response)
            {
                param = JSON.parse(response);
                $(id_span_message).html("<p>" + param.message + "</p>");
                if (param.lignedoc != "")
                {
                    newline = $(param.lignedoc);
                    newline.on('click', '.doc-creer', doc_creer);
                    newline.on('click', '.doc-demande-valid', doc_demande_valid);
                    newline.on('click', '.doc-diffuser', doc_diffuser);
                    newline.on('click', '.doc-scan', doc_scan);
                    newline.on('click', '.doc-supprimer', doc_supprimer);
                    $(ligne_id).replaceWith(newline);
                }
                else
                {
                    $("#liste-scan tr:nth-child(n+2)").remove();
                    $("#liste-scan").append(param.filename);
                    $("#liste-scan").on('click', ".del-scan", del_scan);
                }
                if (param.remove !== undefined)
                    $(param.remove).remove();
            },
            error: function()
            {
                $(id_span_message).html("<span class='erreur'>Erreur jQuery post</span>");
            }
        });
    }

    // bouton pour envoyer un document basé sur un modèle prédéfini
    $(".doc-scan").click(doc_scan);
    function doc_scan(e)
    {
        e.preventDefault();
        $(this).parent().find(".message").html("");
        dialog = $(this).parent().children(".dialog");
        dialog.dialog(
        {
            autoOpen: false,
            height: 400,
            width: 550,
            modal: true,
    //        create: function() { $(this).on('click', '.ajax-save-file', save_file); },
            buttons:
            {
                "Déposer": function() { save_file(dialog.find("form")); },
                "Fermer": function() { dialog.dialog("destroy");}
            },
            close: function()
            {
                //form[ 0 ].reset();
                allFields.removeClass( "ui-state-error" );
            }
        });
        dialog.dialog("open");
    }

    // Supression d'un fichier téléversé
    $(".del-scan").click(del_scan);
    function del_scan(e)
    {
        e.preventDefault();
        id_span_message = "#" + $(this).attr("data-message");
        id_conteneur = "#" + $(this).attr("data-conteneur");
        session_id = $(this).attr("data-sessionid");
        md5sum = $(this).attr("data-md5sum");
        
        jQuery.post
        ({
            url: ajaxurl,
            data:
            {
                'action': 'delete_scan_file',
                'session_id': session_id,
                'md5sum': md5sum,
            },
            success: function(response)
            {
                param = JSON.parse(response);
                $(id_span_message).append("<p>" + param.message + "</p>");
                $(id_conteneur).remove();
            },
            error: function()
            {
                $(id_span_message).html("<span class='erreur'>Erreur jQuery post</span>");
            }
        });
    }

    /*
    $(".doc-compile").click(function()
    {
        session = $(this).attr("data-sessionid");
        user = $(this).attr("data-userid");
        doc = $(this).attr("data-docid");
        resultat = "#" + $(this).attr("data-resultat");
        
        $(resultat).html("Patientez...");
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'export_pdf',
                'user_id': user,
                'session_id': session,
                'doc_id': doc,
            },
            function(response)
            {
                $(resultat).html(response);
            }
        );
        
    });
    */

});
