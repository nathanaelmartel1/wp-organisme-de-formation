jQuery(document).ready(function($)
{
    $(".maj-wpof").click(function(e)
    {
        e.preventDefault();
        log = $("#log");
        bouton = $(this);
        
        jQuery.post
        (
            ajaxurl,
            {
                'action' : bouton.attr('data-action'),
                'param' : bouton.attr('data-param'),
            },
            function(response)
            {
                p = JSON.parse(response);
                log.html(p.log);
            },
        );
    });
    
    $(".insert_in_editor").click(function(e)
    {
        e.preventDefault();
        
        supclass = $(this).attr("data-class");
        if (supclass === undefined)
            supclass = "";
        
        editor = tinyMCE.get($(this).attr('data-editor'));
        if ($(this).attr('data-testsign') === undefined)
            editor.execCommand('mceInsertContent', false, "<span class=\"keyword " + supclass + "\">" + $(this).text() + "</span> ");
        else
            editor.execCommand('mceInsertContent', false, "<span class=\"test " + supclass + "\">operande1 " + $(this).attr('data-testsign') + " operande2 | texte_si_vrai | texte_si_faux</span> ");
    });
    
    $(".clean-modele").click(function(e)
    {
        e.preventDefault();
        
        editor = tinyMCE.get($(this).attr('data-editor'));
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'clean_modele',
                'content': editor.getContent(),
            },
            function(response)
            {
                if (response !== undefined)
                    editor.setContent(response);
            }
        );
    });
    
    $(".ckw_switch_type").change(function(e)
    {
        e.preventDefault();
        
        key = $(this).attr('data-key');
        parent = $("#"+key);
        type = $(this).val();
        jQuery.post
        (
            ajaxurl,
            {
                'action': 'modele_custom_keywords_defaut',
                'key': key,
                'type': type,
                'defaut': '',
            },
            function(response)
            {
                new_line = $(response);
                parent.find('.defaut').html(new_line);
                parent.find('.dynamic-dialog').on('click', dynamic_dialog);
            }
        )
    });
    
    $(".ckw.delete").click(function(e)
    {
        tr = $(this).closest('tr');
        editor = tinyMCE.get('content');
        content = editor.getContent();
        keyword = tr.attr('id');
        keyword_complet = "document:" + keyword;
        
        keyword_in_text = editor.dom.select(".keyword:contains(" + keyword_complet + ")");
        if (keyword_in_text.length > 0)
        {
            tinywindow = editor.getWin();
            tinywindow.alert('Le mot-clé ' + keyword_complet + ' est présent ' + keyword_in_text.length + ' fois dans le texte.\nSupprimez toute occurence pour supprimer le mot-clé');
            tinywindow.getSelection().selectAllChildren(keyword_in_text[0]);
            keyword_in_text[0].scrollIntoView("center");
            editor.focus();
        }
        else
        {
            jQuery.post
            (
                ajaxurl,
                {
                    'action': "ckw_supprime",
                    'ckw': keyword,
                    'modele_id': $('#post_ID').val(),
                },
                function(response)
                {
                    param = JSON.parse(response);
                    if (param.erreur === undefined)
                        tr.remove();
                    else
                        alert('Problème de suppression du mot-clé ' + keyword_complet + ' : ' + param.erreur);
                }
            );
        }
    });
    
    $("table.predefini select").change(function(e)
    {
        dest_td = $(this).closest('tr').find('td.editor');
        if ($(this).val() == 'inactif')
            dest_td.hide();
        else
            dest_td.show();
    });
    
    $(".color-picker").wpColorPicker({ palettes: false, defaultColor: $(this).val() });
    
});
