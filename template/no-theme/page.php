<?php
/**
 * Template simplifiée pour les pages spéciales d'OPAGA
 */
remove_filter( 'the_content', 'wpautop' );
?>

<!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>><!-- OPAGA -->
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <?php wp_head(); ?>
    </head>

    <body <?php body_class("opaga"); ?>>
        <?php wp_body_open(); ?>
        <header id="site-header" role="banner">
            <span class="opaga-board-link icone-bouton"><a href="<?php echo home_url().'/'.$wpof->url_user.'/'; ?>"><?php _e("Tableau de bord"); ?></a></span>
            <div id="site-title">
                <a href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_html(get_bloginfo('name')); ?>" rel="home"><?php echo esc_html(get_bloginfo('name')); ?></a>
            </div>
            <div id="site-description"><?php bloginfo( 'description' ); ?>
            </div>
        </header>

    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php
    
    if (have_posts())
        while (have_posts())
        {
            the_post();
            ?>
            <div class="entry-header">
            <h1 class="entry-title"><?php the_title(); ?></h1>
            </div>
            
            <div class="entry-content">
            <?php the_content(); ?>
            </div>
            <?php
        }
    ?>
    </article>

    <?php show_message_box(); ?>
    <?php if (debug) show_global_log(); ?>
    <footer>
    <?php wp_footer(); ?>
    </footer>
</body>
</html>
