<?php
/*
 * class-modele.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

// Inclure HtmlDomParser installé via composer
require_once wpof_path . '/vendor/autoload.php';
use voku\helper\HtmlDomParser;

class Modele
{
    public $titre = "";
    public $slug = "";
    public $contenu = "";
    
    public $contexte = 0;
    public $signature = 0;
    
    public $orientation = "portrait";
    
    public $ckw = array(); // custom keywords
    
    public $id;
    
    public function __construct($modele_id = -1)
    {
        $this->id = $modele_id;
        
        if ($modele_id > 0)
        {
            $data = get_post($modele_id);
            $meta = get_post_meta($modele_id);
            
            // infos issues du post
            $this->titre = $data->post_title;
            $this->slug = $data->post_name;
            $this->contenu = $data->post_content;
            
            // metadonnées
            foreach($meta as $key => $val)
            {
                if (substr($key, 0, 4) == "ckw_")
                    $this->ckw[substr($key, 4)] = json_decode(stripslashes($val[0]));
                elseif (substr($key, 0, 1) != "_")
                    $this->$key = $val[0];
            }
        }
    }
    
}

function get_modele_by_id($id)
{
    global $Modele;
    
    if (!isset($Modele[$id]))
        $Modele[$id] = new Modele($id);
        
    return $Modele[$id];
}

function get_modele_by_slug($slug)
{
    global $wpdb;
    $query = $wpdb->prepare("SELECT ID FROM {$wpdb->prefix}posts WHERE post_type = 'modele' AND post_status = 'publish' AND post_name = '%s';", $slug);
    $modele_id = $wpdb->get_var($query);
    return get_modele_by_id($modele_id);
}

function get_all_modeles()
{
    global $Modele;
    global $wpdb;
    
    $modele_id = $wpdb->get_col("SELECT ID FROM {$wpdb->prefix}posts WHERE post_type = 'modele' AND post_status = 'publish';", 0);
    foreach($modele_id as $id)
        $Modele[$id] = new Modele($id);
}

add_action('wp_ajax_clean_modele', 'clean_modele');
function clean_modele($content = null)
{
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
        $content = stripslashes($_POST['content']);
    
    $html = preg_replace('/\{([a-z]+:[^}]+)\}/', '<span class="keyword">$1</span>', $content);
    
    $document_html = HtmlDomParser::str_get_html($html);
    
    foreach($document_html->findMulti('.keyword .keyword') as $var)
        $var->outertext = $var->innertext;
    
    foreach($document_html->findMulti('.keyword') as $var)
        $var->innertext = trim($var->innertext);
        
    foreach($document_html->findMulti('.test') as $var)
        $var->innertext = join('|', array_map('trim', explode('|', $var->innertext)));
    
    if (isset($_POST['action']) && $_POST['action'] == __FUNCTION__)
    {
        echo $document_html->save();
        die();
    }
    else
        return $document_html->save();
}

function check_custom_keywords($content)
{
    global $post, $wpof;
    
    $document_html = HtmlDomParser::str_get_html($content);
    
    foreach($document_html->findMulti('.custom_keyword') as $var)
    {
        $var->innertext = trim($var->innertext);
        $vars = array_map('trim', explode(':', $var->innertext));
        if (count($vars) == 1)
            $vars[1] = $vars[0];
        if ($vars[0] != "document")
            $vars[0] = "document";
        if ($vars[1] != "personnalisable")
        {
            $var->class = str_replace('custom_keyword', '', $var->class);
            $var->class = str_replace('erreur', '', $var->class);
            $vars[1] = str_replace('-', '_', sanitize_title($vars[1]));
            update_post_meta($post->ID, "ckw_".$vars[1], str_replace('KEYWORD', $vars[1], $wpof->custom_keyword->template));
            $var->innertext = join(":", $vars);
        }
        else
            $var->class .= " erreur";
    }
    return $document_html->save();
}

add_action('wp_ajax_ckw_supprime', 'ckw_supprime');
function ckw_supprime()
{
    $reponse = array();
    
    $meta_key = "ckw_".$_POST['ckw'];
    if (delete_post_meta($_POST['modele_id'], $meta_key) === false)
        $reponse["erreur"] = __("Impossible de supprimer ce mot-clé (delete_post_meta)");
    
    echo json_encode($reponse);
    die();
}
