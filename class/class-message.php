<?php
/*
 * class-message.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class Message
{
    public  $from_email = "";
    public  $to_email = array();
    private $prefix = "";
    private $subject = "";
    private $content = "";
    private $headers = array();
    private $attachments = array();
    
    private $template;
    
    public function __construct($args = array())
    {
        foreach($args as $key => $val)
        {
            $method = "set_".$key;
            if (method_exists(__CLASS__, $method))
                $this->$method($val);
        }
    }
    
    public function __toString()
    {
        $message = "";
        $message .= "From: ".$this->from_email;
        $message .= "To: ".implode(', ', $this->to_email);
        $message .= "Subject: ".$this->subject;
        $message .= "Content: ".$this->content;
        
        return $message;
    }
    
    public function set_from($from)
    {
        // On part du principe qu'il ne peut y avoir qu'une seule adresse d'expédition
        $this->headers[] = "From: ".reset($this->get_email($from));
    }
    
    public function set_to($to)
    {
        $this->to_email = $this->get_email($to);
    }
    
    public function set_subject($subject)
    {
        $this->subject = $this->prefix." ".$subject;
    }
    
    public function set_content($content)
    {
        $this->content = $content;
    }
    
    public function set_replyto($replyto)
    {
        foreach($this->get_email($replyto) as $email)
            $this->headers[] = "Reply-To: ".$email;
    }
    
    public function set_attachment($file)
    {
        if (!is_array($file))
            $file = array($file);
        $this->attachments = $file;
    }
    
    /*
     * Analyse de l'argument passé
     * $args est un tableau ou une liste de termes séparées par des virgules
     * $args peut contenir des id (on crée l'user correspondant) ou des emails (on copie l'email telle quelle)
     * Tout ce qui n'est ni une email, ni un id d'un vrai user est ignoré
     * 
     * On renvoie un tableau d'email
     */
    private function get_email($args)
    {
        $res = array();
        if (!is_array($args))
            $args = explode(',', str_replace(' ', '', $args));
        foreach($args as $arg)
        {
            if (filter_var($arg, FILTER_VALIDATE_EMAIL))
                $res[] = $arg;
            else if (is_numeric($arg))
            {
                $user = $this->get_user($arg);
                if ($user)
                {
                    $res[] = $user->user_email;
                }
            }
        }
        return $res;
    }
    
    public function get_user($arg)
    {
        $user = null;
        if (is_numeric($arg))
            $user = get_user_by('id', $arg);
        elseif (substr_count($arg, '@') > 0)
            $user = get_user_by_email($arg);
        else
            throw new Exception("Impossible d’identifier l’utilisateur à partir de ".$arg.". Utilisez l’id ou l’email ou rien.");
        
        return $user;
    }
    
    public function sendmail()
    {
        if (empty($this->to_email))
            throw new Exception("Impossible d’envoyer le message : pas de destinataire.");
        
        return wp_mail($this->to_email, $this->subject, $this->content, $this->headers, $this->attachments);
    }
}
