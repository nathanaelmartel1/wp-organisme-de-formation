<?php
/*
 * class-session-formation.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
require_once(wpof_path . "/class/class-formation.php");
require_once(wpof_path . "/class/class-client.php");
require_once(wpof_path . "/class/class-lieu.php");
require_once(wpof_path . "/class/class-document.php");
require_once(wpof_path . "/class/class-upload.php");
require_once(wpof_path . "/class/class-session-stagiaire.php");
require_once(wpof_path . "/class/class-quiz.php");
require_once(wpof_path . "/class/class-creneau.php");
require_once(wpof_path . "/class/class-token.php");


$type_emargement_text = array
(
    'jour' => __("par jour"),
    'tous' => __("tous les stagiaires inscrits à la session"),
    'vide' => __("feuilles vierges"),
    'stagiaire' => __("par stagiaire"),
);

$global_log = array();


class SessionFormation
{
    public $numero = ""; // nomenclature interne
    public $titre_session = "";
    public $titre_formation = "";
    public $session_unique_titre = "";
    public $permalien = "";
    public $slug = "";
    public $bandeau_id = "-1"; // image mise en avant
    public $acces_session;
    
    // tableau d'ID des formateurs
    public $formateur = array();
    
    // Infos venant de la formation catalogue
    public $duree = 0;
    public $tarif_inter = 0;
    public $tarif_intra = 0;
    
    // infos effectives pour cette session
    public $tarif_heure = 0;
    public $nb_heure; // au format horaire : 3:30
    public $nb_heure_decimal = 0; // nombre d'heures au format décimal : 3,5 pour 3:30
    public $nb_jour = "";
    public $tarif_base_total = false; // détermine si le tarif de base défini pour le total (true) ou à l'heure (false)
    public $tarif_total_chiffre = 0;
    public $tarif_total_lettre = 0;
    public $budget_global = 0; // montant de la facture ou total des factures en inter
    public $exe_comptable = array(); // les clés sont la ou les années, les valeurs sont le ou les budgets de l'année concernée
    
    public $quizpr = "";
    public $quizobj = "";
    public $quizpr_id = "";
    public $quizobj_id = "";
    
    public $dates_array = array();
    public $dates_texte = "";
    public $first_date = "";
    public $first_date_timestamp = 0;
    
    // vaut 1 si la session a bien été réalisée
    public $realisee = 0;
    
    public $type_emargement = array();
    
    // type de session
    public $type_index = "";
    public $type_texte = "";
    
    // Objet lieu
    public $lieu = -1;
    public $lieu_nom = "";
    public $lieu_adresse = "";
    public $lieu_code_postal = "";
    public $lieu_ville = "";
    public $lieu_localisation = "";
    public $lieu_secu_erp = "";
    
    // Clients
    public $clients = array();
    
    // stagiaires
    public $stagiaires_min = 0;
    public $stagiaires_max = 0;
    public $inscrits = array();
    public $stagiaires = array(); // tableau de SessionStagiaire
    public $nb_stagiaires = 0;
    public $nb_confirmes = 0;
    
    // documents
    public $doc_suffix;
    public $doc_necessaire = array(); // identifiant des docs globaux nécessaires pour cette session
    public $uploads = array(); // tableau des documents scannés ou extérieurs, attachés à cette session
    
    public $id;
    
    public $date_modif = "";
    
    // ID de la formation en doublon pour assurer la compatibilité
    public $formation_id = -1;
    public $formation = -1;
    
    public $creneaux = array();
    
    
    public function __construct($session_id = -1)
    {
        global $Formation;
        global $wpof;
        
        $this->tarif_inter = $wpof->tarif_inter;
        
        log_add(__METHOD__."($session_id)", "session");
        if ($session_id > 0)
        {
            global $SessionFormation;
            $SessionFormation[$session_id] = $this;
            
            $this->id = $session_id;
            $wp_post = get_post($session_id);
            $meta = get_post_meta($session_id);
            
            // date de dernière modif
            if (isset($meta['timestamp_modif'][0]))
                $this->date_modif = date_i18n("j F Y", $meta['timestamp_modif'][0]);
            
            // caractéristiques de la formation
            $this->specialite = (isset($meta['specialite'][0])) ? $meta['specialite'][0] : "";
            
            // la session a été réalisée
            $this->realisee = (isset($meta['realisee'][0])) ? $meta['realisee'][0] : "";
            
            foreach($wpof->desc_formation->term as $k => $t)
                $this->$k = (isset($meta[$k][0])) ? $meta[$k][0] : "";
            
            // infos issues des meta données du post
            if (isset($meta['formation'][0]))
            {
                $this->formation_id = $this->formation = $meta['formation'][0];
                
                if (!isset($Formation[$this->formation]))
                    $Formation[$this->formation] = new Formation($meta['formation'][0]);

                $formation = $Formation[$this->formation];
                
                foreach($wpof->desc_formation->term as $k => $t)
                {
                    if ($wpof->{"formation_".$k."_mode"} == 'force')
                        $this->$k = $wpof->{"formation_".$k."_text"};
                    elseif ($this->$k == "")
                        $this->$k = $formation->$k;
                }
                        
                if ($this->titre_formation == "")
                    $this->titre_formation = $formation->titre;

                $this->tarif_inter = $formation->tarif;
                $this->titre_formation = $formation->titre;
                
                if (empty($this->date_modif))
                    $this->date_modif = $formation->date_modif;
            }
            if (isset($meta['session_unique_titre'][0]) && $meta['session_unique_titre'][0] != "")
                $this->session_unique_titre = $this->titre_formation = $meta['session_unique_titre'][0];
                
            // récupération des bons identifiants de quiz et du parent
            if (isset($meta['quizpr_id'][0]))
            {
                $this->quizpr_id = $meta['quizpr_id'][0];
                $this->quizpr_parent_id = (isset($meta['quizpr_parent_id'][0])) ? $meta['quizpr_parent_id'][0] : $this->id;
                $this->quizpr = new Quiz($this->quizpr_id);
            }
            
            if (isset($meta['quizobj_id'][0]))
            {
                $this->quizobj_id = $meta['quizobj_id'][0];
                $this->quizobj_parent_id = (isset($meta['quizobj_parent_id'][0])) ? $meta['quizobj_parent_id'][0] : $this->id;
                $this->quizobj = new Quiz($this->quizobj_id);
            }

            $this->formateur = get_post_meta($session_id, "formateur", true); // permet de récupérer l'info sous forme de tableau
            if (!is_array($this->formateur))
                $this->formateur = array();
            
            // type de session de formation
            if (isset($meta['type_formation'][0]))
            {
                $this->type_index = $meta['type_formation'][0];
                $this->type_texte = $wpof->type_session[$this->type_index]['value'];
            }
            
            // Visibilité de la session
            $this->visibilite_session = (isset($meta['visibilite_session'][0])) ? $meta['visibilite_session'][0] : 'public';
            $this->acces_session = (isset($meta['acces_session'][0])) ? ($meta['acces_session'][0]) : $this->visibilite_session;

            // lieu
            $lieu = null;
            if (isset($meta['lieu'][0]))
            {
                $this->lieu = $meta['lieu'][0];
                $lieu = get_lieu_by_id($this->lieu);
            }
                
            foreach(array("nom", "adresse", "code_postal", "ville", "localisation", "secu_erp") as $data)
            {
                $lieu_data = 'lieu_'.$data;
                if ($lieu && isset($lieu->$data))
                    $this->$lieu_data = $lieu->$data;
                elseif (isset($meta[$lieu_data][0]))
                    $this->$lieu_data = $meta[$lieu_data][0];
            }
            $this->ville = $this->lieu_ville;
            
            // stagiaires
            $this->clients = get_post_meta($session_id, 'clients', true); // tableau
            if (!is_array($this->clients))
                $this->clients = array();
            else
            {
                // on inscrit le numéro du premier client en tant que numéro de session pour classer par ce critère
                if (champ_additionnel('numero_contrat'))
                    $this->numero = get_client_meta(reset($this->clients), "numero_contrat");
            }
            
            $this->stagiaires_min = (isset($meta['stagiaires_min'][0])) ? $meta['stagiaires_min'][0] : 0;
            $this->stagiaires_max = (isset($meta['stagiaires_max'][0])) ? $meta['stagiaires_max'][0] : 0;
            $this->inscrits = get_post_meta($session_id, 'inscrits', true); // tableau
            if (!is_array($this->inscrits))
                $this->inscrits = array();
            
            // types de feuilles d'émargement
            $this->type_emargement = get_post_meta($session_id, 'type_emargement', true);
            if ($this->type_emargement == "")
            {
                global $type_emargement_text;
                $this->type_emargement = array_fill_keys(array_keys($type_emargement_text), 0);
                $this->type_emargement['jour'] = 1;
            }
            
            // documents nécessaires
            foreach($wpof->documents->term as $doc_index => $doc)
            {
                if ($doc->contexte & $wpof->doc_context->session)
                    $this->doc_necessaire[] = $doc_index;
            }
            $this->doc_suffix = $this->id;
            
            /*
            if ($this->type_index != "")
            {
                global $doc_nom;
                foreach (array_keys($doc_nom) as $doc_index)
                {
                    if ($doc_index == "pv_secu" && ($lieu) && $lieu->secu_erp == "")
                        continue;

                    if ($wpof->{$doc_index."_global_".$this->type_index} == 1)
                        $this->doc_necessaire[] = $doc_index;
                }
            }
            */
            
            // scans et cie
            $this->uploads = unserialize(get_post_meta($this->id, "uploads", true));
            if (!is_array($this->uploads))
                $this->uploads = array();
            
            $this->init_dates_creneaux();
            
            if (!empty($meta['nb_heure_estime_decimal'][0]))
                $this->nb_heure_estime_decimal = $meta['nb_heure_estime_decimal'][0];
            
            $this->calcule_temps_session();
            
            // renseignement du lieu si pas de lieu défini et que seuls des créneaux à distance sont définis
            $this->init_lieu_by_creneaux();

            
            if (isset($meta['tarif_total_chiffre'][0]))
            {
                $this->tarif_total_chiffre = $meta['tarif_total_chiffre'][0];
                $this->calcule_tarif();
            }
            
            // infos issues du post
            $this->titre_session = $wp_post->post_title;
            
            // Vérification de la conformité du post_title
            //$this->check_post_title($wp_post);
            
            $this->permalien = get_the_permalink($session_id);
            $this->slug = $wp_post->post_name;
        }
        else
        {
            foreach(array_keys($wpof->desc_formation->term) as $k)
                $this->$k = "";
        }
    }
    
    /*
     * Qui peut modifier cette session ?
     * Renvoie true si
     * $user_id a le rôle responsable ou admin
     * $user_id fait partie des formateurs animant cette session ($this->formateur)
     * $this->formateur est vide (session non définie)
     */
    public function can_edit($user_id = -1)
    {
        global $wpof;
        
        if ($user_id == -1)
            $user_id = get_current_user_id();
        
        $role = wpof_get_role($user_id);
        $super_roles = array("admin", "um_responsable");
        
        if ($role == "um_stagiaire" || $role == null)
            return false;
        
        if (!isset($this->formateur)
            || !is_array($this->formateur)
            || count($this->formateur) == 0
            || in_array($user_id, $this->formateur)
            || in_array($role, $super_roles))
            return true;
        
        return false;
    }
    
    /*
     * Création des sessions stagiaires
     * En effet, on n'a pas toujours besoin des sessions stagiaires lorsqu'on instancie une session formation
     */
    public function init_stagiaires()
    {
        global $Client;
        $this->nb_stagiaires = $this->nb_confirmes = 0;
        
        if (!isset($Client[reset($this->clients)]))
            $this->init_clients();
        foreach($this->clients as $c)
        {
            $this->nb_stagiaires += $Client[$c]->nb_stagiaires;
            $this->nb_confirmes += $Client[$c]->nb_confirmes;
        }
    }
    
    public function init_clients($client_id = -1)
    {
        global $Client;
        
        if ($client_id > 0)
            $client_list = array($client_id);
        else
            $client_list = $this->clients;
        
        foreach($client_list as $cid)
            if (!isset($Client[(integer)$cid]))
                $Client[(integer)$cid] = new Client($this->id, $cid);
            
        $this->calcule_budget_global();
    }
    
    /*
     * Initialisation des dates et des créneaux
     */
    public function init_dates_creneaux()
    {
        $tmp_creneaux = get_post_meta($this->id, "creneaux", true);
        
        if (!is_array($tmp_creneaux) || empty($tmp_creneaux))
            $this->dates_array = $this->creneaux = array();
        else
        {
            foreach($tmp_creneaux as $date => $jour_creneaux)
            {
                if (is_array($jour_creneaux) && count($jour_creneaux) > 0)
                    foreach ($jour_creneaux as $c)
                    {
                        $creno = new Creneau($c);
                        if ($creno->lieu_id == -1)
                            $creno->lieu_nom = $this->lieu_nom;
                        $this->creneaux[$date][$c] = $creno;
                    }
                else
                    $this->creneaux[$date] = array();
            }
            $this->dates_array = array_keys($this->creneaux);
            $this->dates_texte = pretty_print_dates($this->dates_array);
            $this->first_date = $this->dates_array[0];
            $this->first_date_timestamp = date_create_from_format("d/m/Y", $this->first_date)->getTimestamp();
        }
    }
    
    private function init_lieu_by_creneaux()
    {
        if ($this->lieu_ville == "")
        {
            $type_creneau = array();
            foreach($this->creneaux as $date)
                foreach($date as $c)
                    $type_creneau[$c->type] = 1;
            
            if (!empty($type_creneau) && !isset($type_creneau['presentiel']) && !isset($type_creneau['afest']))
                $this->lieu_nom = __("distance");
        }
    }
    
    public function get_id_link()
    {
        return "<a href='{$this->permalien}' title='{$this->titre_session}'>{$this->id}</a>";
    }
    
    public function get_client_by_stagiaire($stagiaire_id)
    {
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            if (in_array($stagiaire_id, $client->stagiaires))
                return $client;
        }
        return null;
    }
    
    public function check_post_title($wp_post = null)
    {
        if (!$wp_post)
            $wp_post = get_post($this->id);
        
        $concat_id = true;
        $actuel_titre_session = $this->titre_formation;
        if (!empty($this->first_date))
        {
            $actuel_titre_session .= " – ".pretty_print_dates($this->first_date);
            $concat_id = false;
        }
        /*
        if (!empty($this->lieu_ville))
        {
            $actuel_titre_session .= " – ".$this->ville;
        }
        */
        $actuel_titre_session = strip_tags($actuel_titre_session);
        
        $slug = sanitize_title($actuel_titre_session);
        if ($concat_id)
            $slug .= "-".$this->id;
        
        if ($actuel_titre_session != $this->titre_session)
        {
            $new_data = array
            (
                'ID' => $this->id,
                'post_title' => $actuel_titre_session,
                'post_name' => $slug,
            );
            remove_action( 'post_updated', 'wp_save_post_revision' );
            $res = wp_update_post(wp_slash($new_data), true);
            add_action( 'post_updated', 'wp_save_post_revision' );
            if ($res != $this->id)
                var_dump($res);
            $this->titre_session = $new_data['post_title'];
            
            return true;
        }
        return false;
    }

    
    /*
     * Surcharge de update_post_meta
     * Peut être appelée dans un contexte où l'on veut mettre à jour une donnée sans se soucier de savoir
     * si l'on est sur une SessionFormation ou une SessionStagiaire
     */
    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpof;
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        if ($meta_key == "creneaux")
            return $this->update_creneaux();
            
        if (in_array($meta_key, array_keys($wpof->desc_formation->term)))
        {
            wp_update_post(array('ID' => $this->id));
            update_post_meta($this->id, "timestamp_modif", time());
        }
        return update_post_meta($this->id, $meta_key, $meta_value);
    }
    
    // Cas particulier des créneaux où le stocke que les index
    private function update_creneaux()
    {
        // initialisation de tmp_creneaux avec les dates triées en clés
        $tmp_creneaux = array_fill_keys(sort_dates(array_keys($this->creneaux)), array());
        
        foreach($this->creneaux as $date => $jour_creneaux)
        {
            if (is_array($jour_creneaux) && count($jour_creneaux) > 0)
            {
                foreach($jour_creneaux as $c)
                    $tmp_creneaux[$date][] = $c->id;
            }
            else
                $tmp_creneaux[$date] = array();
        }
        
        return update_post_meta($this->id, "creneaux", $tmp_creneaux);
    }
    
    public function delete()
    {
        $log = array();
        // suppression des clients et stagiaires
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            $client->delete();
            
            $log[] = "Suppression de ".$client->nom;
        }
        
        global $wpdb;
        
        // suppression des créneaux
        global $suffix_creneaux;
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix.$suffix_creneaux." WHERE session_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression des quiz
        global $suffix_quiz;
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix.$suffix_quiz." WHERE parent_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression des documents
        global $suffix_documents;
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix.$suffix_documents." WHERE session_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression des metas
        $query = $wpdb->prepare ("DELETE FROM ".$wpdb->prefix."postmeta WHERE post_id = '%d';", $this->id );
        $res = $wpdb->query($query);
        $log[] = $query." → ".$res;
        
        // suppression du post (sans passer par la case poubelle)
        wp_delete_post($this->id, true);
        
        return $log;
    }

    public function get_delete_bouton($texte_bouton = "Supprimer cette session")
    {
        ob_start();
        ?>
        <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->id; ?>">
        <span class="dashicons dashicons-dismiss" > </span>
        <?php echo $texte_bouton; ?>
        </p>
        <?php
        return ob_get_clean();
    }

    /*
     * renvoie la liste des stagiaires (prénom nom) sous forme de liste
     * , : séparés par des virgules
     * liste : liste à puce li sans les balises ul ou ol, à rajouter autour de cet appel
     * tableau : tableau tr td sans les balises table, à rajouter autour de cet appel
     * En fait, seule la première lettre est analysée, donc souplesse
     * 
     * Si $confirme est vrai, on ne sort que les stagiaires confirmés
     */ 
    public function get_liste_stagiaires($type_liste = ',', $confirme = false)
    {
        $liste = "";
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            $liste .= $client->get_liste_stagiaires($type_liste, $confirme);
        }
        if ($liste != "")
        {
            switch(substr($type_liste, 0, 1))
            {
                case 't':
                    $liste = "<table>".$liste."</table>";
                    break;
                case 'l':
                    $liste = "<ul>".$liste."</ul>";
                    break;
                case ',':
                default:
                    break;
            }
        }
        return $liste;
    }
    
    public function get_formateurs_noms($format = 'string')
    {
        $formateurs_nom = array();
        foreach($this->formateur as $f_id)
            $formateurs_nom[] = get_displayname($f_id);
        if ($format == 'string')
            return implode(', ', $formateurs_nom);
        else
            return $formateurs_nom;
    }
    
    
    // Crée les inputs pour définir la répartition par exercire comptable
    public function get_input_exe_comptable($exe_comptable = null)
    {
        if (!$exe_comptable) $exe_comptable = $this->exe_comptable;
        
        $html = "<div class='exe_comptable'>";
        $rand = rand();
        
        foreach($exe_comptable as $annee => $tarif)
        {
            $html .= "<label for='".$annee.$rand."'>$annee</label>";
            $html .= "<input id='".$annee.$rand."' name='$annee' type='number' step='0.01' value='$tarif' />";
        }
        $html .= "</div>";
        
        return $html;
    }
    
    
    // Renseigne le tableau des exercices comptables
    // En général ce tableau n'aura qu'une valeur (si toute la session se déroule dans la même année civile)
    public function set_exercice_comptable()
    {
        if ($this->type_index != 'inter' && !empty($this->dates_array))
        {
            $this->exe_comptable = get_post_meta($this->id, "exe_comptable", true);
            
            if (!is_array($this->exe_comptable) || count($this->exe_comptable) == 0 || in_array("", array_keys($this->exe_comptable)))
            {
                $this->exe_comptable = array();
                foreach($this->dates_array as $d)
                {
                    $date = explode('/', $d);
                    $this->exe_comptable[$date[2]] = 0;
                }
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] = $this->tarif_total_chiffre;
                $this->update_meta("exe_comptable");
            }
            $diff = $this->tarif_total_chiffre - array_sum($this->exe_comptable);
            if ($diff != 0)
            {
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] += $diff;
                
                $this->update_meta("exe_comptable");
            }
        }
    }
    
    // Calcule le temps (en heures) de suis de la session par ce stagiaire (somme de tous les créneaux actifs)
    public function calcule_temps_session()
    {
        $this->temps = DateTime::createFromFormat("U", "0");
            
        foreach($this->creneaux as $tab_date)
        {
            if (!empty($tab_date))
            {
                foreach($tab_date as $creno)
                {
                    if ($creno->type != "technique")
                        $this->temps->add($creno->duree);
                }
                $h = floor($this->temps->format("U") / 3600);
                $m = ($this->temps->format("U") % 3600) / 60;
                $this->nb_heure_decimal = $this->temps->format("U") / 3600;
                $this->nb_heure = sprintf("%02d:%02d", $h, $m);
            }
        }
        
        $this->dates_array = array_unique($this->dates_array);
        $this->dates_texte = pretty_print_dates($this->dates_array);
        
        if (!isset($this->nb_heure_estime_decimal))
        {
            $this->nb_heure_estime_decimal = $this->nb_heure_decimal;
            $this->nb_heure_estime = $this->nb_heure;
        }
        else
        {
            $h = (integer) $this->nb_heure_estime_decimal;
            $m = ($this->nb_heure_estime_decimal - $h) * 60;
            $this->nb_heure_estime = sprintf("%02d:%02d", $h, $m);
        }
    }    

    // Calcule le tarif total affiché
    public function calcule_tarif()
    {
        if ($this->nb_heure_decimal > 0)
            $this->tarif_heure = sprintf("%.2f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 2));
        $this->tarif_total_lettre = num_to_letter($this->tarif_total_chiffre);
    }
            
    public function calcule_budget_global()
    {
        global $Client;
        $this->budget_global = 0;
        foreach ($this->clients as $cid)
            $this->budget_global += get_client_meta($cid, "tarif_total_chiffre");
    }
    
    // Supprime un élément d'un tableau de sous-entité (clients, inscrits)
    function supprime_sous_entite($tab_name, $id)
    {
        if (isset($this->$tab_name) && is_array($this->$tab_name))
        {
            $key = array_search($id, $this->$tab_name);
            unset($this->$tab_name[$key]);
            $this->update_meta($tab_name, $this->$tab_name);
        }
    }
    
    /*
     * Création des documents
     * En effet, on n'a pas toujours besoin des documents lorsqu'on instancie une session formation
     */
    public function init_docs()
    {
        global $Documents;
        global $wpof;
        
        foreach ($this->doc_necessaire as $d)
        {
            $doc = new Document($d, $this->id, $wpof->doc_context->session);
            $Documents[$doc->id] = $doc;
        }
    }
    
    public function get_clients($meta = "nom", $format = ',')
    {
        $html = array();
        
        foreach($this->clients as $cid)
        {
            $client = get_client_by_id($this->id, $cid);
            if ($client->financement == "opac")
                $class = "sous-traitance";
            else
                $class = "autre";
            $html[] = "<span class='$class'>".$client->$meta."</span>";
        }
        
        switch (substr($format, 0, 1))
        {
            case 'l':
                $html = "<ul><li>".join("</li><li>", $html)."</li></ul>";
                break;
            case 't':
                $html = "<table><tr><td>".join("</td><td>", $html)."</td></tr></table>";
                break;
            case 'b':
                $html = join('<br />', $html);
                break;
            default:
                $html = join(', ', $html);
                break;
        }
        return $html;
    }
    
    /*
     * Retourne le taux de complétion de la session sous la forme :
     * nb_champs_remplis / nb_champs_a_repmlir
     */
    public function get_session_completion($html = true)
    {
        global $wpof;
        
        $total = count($wpof->completion_session[$this->acces_session]);
        $rempli = 0;
        $vide = array();
        foreach($wpof->completion_session[$this->acces_session] as $k)
        {
            if ($this->$k && trim($this->$k) != "")
                $rempli++;
            elseif ($html)
                $vide[] = $k;
        }
        
        if ($html)
        {
            $class = ($rempli/$total == 1) ? "fait" : "attention";
            return '<span title="'.join('&#013;&#010;', $vide).'" class="'.$class.'">'.$rempli.'/'.$total.'</span>';
        }
        else
            return $rempli/$total;
    }
    
    public function get_session_control_head($tempo = "futur")
    {
        ob_start();
        ?>
        <tr>
        <th><?php _e("Titre session"); ?></th>
        <th class='thin'><?php _e("Complétion description"); echo get_icone_aide("sessionformation_completion_description"); ?></th>
        <th><?php _e("Clients"); ?><br /><?php _e('M : pers. morale / P : pers. physique / S : sous-traitance'); ?></th>
        <th><?php _e("Visibilité agenda"); echo get_icone_aide("sessionformation_acces_session"); ?></th>
        <?php if ($tempo == 'passe') : ?>
        <th class='thin'><?php _e("Réalisée"); echo get_icone_aide("sessionformation_realisee"); ?></th>
        <?php endif; ?>
        <th class='thin'><?php _e("Supprimer"); echo get_icone_aide("sessionformation_supprimer"); ?></th>
        </tr>
        <?php
        return ob_get_clean();
    }
    /*
     * Renvoie une ligne de tableau pour contrôler la session
     * 
     */
    public function get_session_control($tempo = "futur")
    {
        $role = wpof_get_role(get_current_user_id());
        ob_start();
        ?>
        <tr id="session<?php echo $this->id; ?>">
        <td><a href='<?php echo $this->permalien; ?>'><?php echo $this->titre_session; ?></a></td>
        <td class="center"><?php echo $this->get_session_completion(true); ?></td>
        <td class="liste-client">
            <ul>
            <?php
            $nb_stagiaires = 0;
            foreach ($this->clients as $cid)
            {
                $client = get_client_by_id($this->id, $cid);
                $class = 'morale';
                if ($client->entite_client == "physique")
                    $class = 'physique';
                elseif ($client->financement == 'opac')
                    $class = 'sous-traitance';
                
                $client->init_stagiaires();
                echo "<li class='$class'>";
                echo "{$client->nom} ";
                if ($client->financement != "opac" && $client->entite_client == "morale")
                {
                    $ss = ($client->nb_confirmes > 1) ? "s" : "";
                    echo " – ".sprintf(__("%d stagiaire%s confirmé%s / %d"), $client->nb_confirmes, $ss, $ss, $client->nb_stagiaires);
                }
                echo "</li>";
                $nb_stagiaires += $client->nb_confirmes;
            }
            /*
            echo $this->get_clients("nom", "l");
            */
            $nb_clients = count($this->clients);
            $sc = ($nb_clients > 1) ? "s" : "";
            $ss = ($nb_stagiaires > 1) ? "s" : "";
            echo "<p class='right'>$nb_clients ".__("client").$sc." / $nb_stagiaires ".__('stagiaire').$ss."</p>";
            ?>
            </ul>
        </td>
        <td><?php echo get_input_jpost($this, "acces_session", array('select' => '', 'aide' => false));  ?></td>
        <?php if ($tempo == 'passe') : ?>
            <td class="center">
            <?php 
                if ($this->realisee == 1 && !in_array($role, array("um_responsable", "admin")))
                    echo '<span class="succes dashicons dashicons-yes"></span>';
                else
                    echo get_input_jpost($this, "realisee", array('input' => 'checkbox', 'display' => 'inline', 'aide' => false));
            ?>
            </td>
        <?php endif; ?>
        <td class="center">
        <?php if (empty($this->clients)) : ?>
        <p class="delete-entity icone-bouton" data-objectclass="SessionFormation" data-id="<?php echo $this->id; ?>" data-parent="tr#session<?php echo $this->id; ?>" title="<?php _e("Supprimer"); ?>">
        <span class="dashicons dashicons-dismiss" ></span>
        </p>
        <?php endif; ?>
        </td>
        </tr>
        <?php
        return ob_get_clean();
    }
    
    public function get_template()
    {
        global $wpof;
        
        $user_id = get_current_user_id();
        $role = wpof_get_role($user_id);
        
        // Si le visiteur n'est pas connecté ou n'a pas le droit de modification, on affiche le template public
        if ($user_id == 0
            || ($role == "um_formateur-trice" && !in_array($user_id, $this->formateur))) : 
            echo $this->get_html_presentation();
            ?>
                <div class="icone-bouton premier_contact dynamic-dialog" data-function="premier_contact" data-sessionid="<?php echo $this->id; ?>">
                    <span class="dashicons dashicons-email-alt"></span>
                    <?php _e("Inscription et/ou renseignements"); ?>
                </div>
        <?php else : ?>
            <div id="session" class="<?php echo $role;?> session_box">
            
            <?php
                $this->init_clients();
                $this->init_stagiaires();
                $this->the_board();
            ?>
            
            </div> <!-- session_box session -->
        <?php endif;
    }


    /*
     * Renvoie la présentation de la session
     * $args['complet'] : affiche tous les détails
     * $args['entite] : affiche les informations actualisé pour le client ou le stagiaire
     */
    public function get_html_presentation($args = array('complet' => true, 'entite' => null))
    {
        global $wpof, $post;
        
        if (!isset($args['complet']))
            $args['complet'] = true;
        if (isset($args['entite']))
        {
            $object = $args['entite'];
            $object_class = get_class($object);
            if ($object_class == "Client")
                $client = $object;
            else
            {
                $stagiaire = $object;
                $client = get_client_by_id($stagiaire->session_formation_id, $stagiaire->client_id);
            }
            
        }
        else
            $object = null;
        
        ob_start();
        ?>
        <div class="session-global">
        <?php if ($args['complet']) : ?>
        <div class="session-side-content">
            <p><?php printf(__("Programme édité le %s"), $this->date_modif); ?></p>
            <dl>
                <dt><?php _e("Équipe pédagogique"); ?></dt>
                    <dd><?php the_liste_formateur(array('only' => $this->formateur), true); ?></dd>
                <dt><?php _e("Dates"); ?></dt>
                    <dd><?php echo $this->dates_texte; ?></dd>
                    <?php if ($object == null) : ?>
                    <dd><a href="#planning-session"><?php _e("Voir planning détaillé") ?></a></dd>
                    <?php endif; ?>
                <dt><?php _e("Durée"); ?></dt>
                    <dd><?php echo $this->nb_heure; ?></dd>
                <?php if (isset($object) && $object_class != "SessionStagiaire") : ?>
                <dt><?php _e("Tarif"); ?></dt>
                    <?php if (isset($object) && $object_class == "Client") : ?>
                        <dd><?php echo get_tarif_formation($client->tarif_total_chiffre); ?></dd>
                    <?php else : ?> 
                        <dd><?php echo get_tarif_formation($this->tarif_total_chiffre)." ".__("(par stagiaire)"); ?></dd>
                    <?php endif; ?>
                <?php endif; ?>
                    
                <?php if ($this->stagiaires_min * $this->stagiaires_max != 0) : ?>
                <dt><?php _e("Nombre de stagiaires"); $this->init_stagiaires();?></dt>
                    <dd><?php if ($this->stagiaires_min > 0) echo __("minimum")." : ".$this->stagiaires_min; ?></dd>
                    <dd><?php if ($this->stagiaires_max > 0) echo __("maximum")." : ".$this->stagiaires_max; ?></dd>
                    <dd>
                    <?php
                        if ($this->stagiaires_max > 0)
                        {
                            if ($this->nb_confirmes < $this->stagiaires_max)
                                printf(__("il reste %s place%s"), $this->stagiaires_max - $this->nb_confirmes, ($this->stagiaires_max - $this->nb_confirmes > 1) ? "s" : "");
                            else
                                echo "<span class='alerte'>".__("Les inscriptions sont closes")."</span>";
                        }
                    ?>
                    </dd>
                <?php endif; ?>
                
                <?php if ($object != null): ?>
                <dt><?php _e("Nombre de stagiaires"); $this->init_stagiaires(); echo " ".$this->nb_stagiaires."/".$this->nb_confirmes; ?></dt>
                    <dd><strong><?php echo $client->nom; ?></strong> : <?php echo $client->nb_confirmes; ?></dd>
                    <?php if ($client->nb_confirmes < $this->nb_confirmes) : ?>
                    <dd><strong><?php _e("Autres") ?></strong> : <?php echo $this->nb_confirmes - $client->nb_confirmes; ?></dd>
                    <?php endif; ?>
                <?php endif; ?>
                <?php if ($this->lieu_ville != "") : ?>
                <dt><?php _e("Lieu"); ?></dt>
                    <dd><?php echo $this->lieu_nom; ?></dd>
                    <dd><?php echo $this->lieu_adresse; ?></dd>
                    <dd><?php echo $this->lieu_ville; ?></dd>
                <?php endif; ?>
            </dl>
            
            <?php if ($object == null) : ?>
                <div class="icone-bouton get-proposition-pdf">
                <a href="/?download=proposition&s=<?php echo $this->id; ?>&ci=<?php echo $this->id; ?>&c=<?php echo $wpof->doc_context->session;?>">
                <span class="dashicons dashicons-pdf"></span>
                <?php _e("Cette présentation en PDF"); ?>
                </a>
                </div>
                <div class="icone-bouton dynamic-dialog" data-function="premier_contact" data-sessionid="<?php echo $this->id; ?>">
                    <span class="dashicons dashicons-email-alt"></span>
                    <?php _e("Inscription et/ou renseignements"); ?>
                </div>
                <div class="icone-bouton dynamic-dialog" data-function="new_token_form" data-sessionid="<?php echo $this->id; ?>">
                    <span class="dashicons dashicons-admin-network"></span>
                    <?php _e("Obtenir un nouveau lien d'accès"); ?>
                </div>
            <?php endif; ?>
        </div>
        <?php endif; ?>
        
        <div class='session-content'>
        <?php if ($args['complet']) : ?>
        <dl>
        <?php
            $desc_proposition = $wpof->desc_formation->get_group("proposition");
            foreach($desc_proposition->term as $k => $t)
            {
                ?>
                <dt><?php echo $t->text; ?></dt>
                <dd><?php echo wpautop($this->$k); ?></dd>
                <?php
            }
        ?>
        <?php if ($object == null) : ?>
            <dt id="planning-session"><?php _e("Planning de la session"); ?></dt>
                <dd><?php echo $this->get_decoupage_temporel(); ?></dd>
        <?php endif; ?>
        </dl>
        <?php
            else :
                echo wpautop($this->presentation);
            endif;
        ?>
        </div> <!-- session-content -->
        
        </div> <!-- session-global -->
        <?php 
        return ob_get_clean();
    }
    
    public function get_the_stagiaire_board($stagiaire_id)
    {
        global $SessionStagiaire;
        ob_start();
        ?>
        <div class='board-nom-stagiaire' id='tab-s<?php echo $stagiaire_id; ?>'>
        <?php echo get_stagiaire_displayname($stagiaire_id); ?>
        <span class='last-modif'>(<?php echo __("Dernière connexion")." ".get_last_login($stagiaire_id).")"; ?></span>
        <?php
        $SessionStagiaire[$stagiaire_id]->the_board();
        ?>
        </div>
        <?php
        return ob_get_clean();
    }
    
    public function the_stagiaire_board($user_id)
    {
        echo $this->get_the_stagiaire_board($user_id);
    }

    /*
    * Tableau de bord du formateur pour une session donnée
    * $session_id : ID de la session
    * $user_id : ID du stagiaire
    * $formation_id : ID de la formation
    * $inscrits : tableau contenant les ID des stagiaires inscrits
    */
    public function the_board()
    {
        global $Formation;
        global $SessionStagiaire;
        global $Documents;
        global $wpof, $global_log;
        
        $user_id = get_current_user_id();
        $role = wpof_get_role($user_id);
        $global_log[] = var_export($wpof->test_modele->get_fields("sign"), true);
        
        $this->init_docs();
        
        ?>
        <div id="session-formation" class="id session" data-id="<?php echo $this->id; ?>">
        <?php 
        $default_main_tab = 0;
        if (isset($_SESSION['main-tabs']))
            $default_main_tab = $_SESSION['main-tabs'];
        elseif (count($this->clients) == 0)
            $default_main_tab = 1;
        
        echo hidden_input("default_main_tab", $default_main_tab);
        echo get_icone_aide("aide_session_globale", __("Pour gérer cette session"));
        ?>
        </div>
        
        <div id="main-tabs">
        <ul>
            <li><a href="#tab-client"><?php _e("Clients/stagiaires"); ?></a></li>
            <li><a href="#tab-session"><?php _e("Session"); ?></a></li>
            <li><a href="#tab-formation"><?php _e("Formation"); ?></a></li>
            <li><a href="#tab-dates"><?php _e("Dates"); ?></a></li>
            <li><a href="#tab-lieu"><?php _e("Lieu"); ?></a></li>
            <li><a href="#tab-eval"><?php _e("Évaluations"); ?></a></li>
            <li><a href="#tab-token"><?php _e("Accès privés"); ?></a></li>
        </ul>
        
        <div id="tab-session">
        <div class="infos-session flexrow edit-data" data-id="<?php echo $this->id; ?>">
        <fieldset>
            <legend><?php _e("Formation"); ?></legend>
        <?php
            if ($this->formation_id > 0)
            {
                $formation = get_formation_by_id($this->formation_id);
                printf(__("Basée sur la formation catalogue : <strong><a href='%s'>%s</a></strong>"), $formation->permalien, $formation->titre); ?>
                <a href="<?php echo $formation->permalien.'?'.$wpof->formation_edit_link_suffix; ?>" target="_blank"><span class="icone-bouton dashicons dashicons-edit"></span></a>
                <?php
            }
            ?>
            <?php
            echo get_input_jpost($this, "acces_session", array('select' => '', 'label' => __("Accès à la session (visibilité dans l'agenda)")));
            if (!isset($wpof->formation))
                init_term_list("formation");
            echo get_input_jpost($this, "session_unique_titre", array('input' => 'text', 'label' => __("Intitulé de session unique"), 'size' => '80', 'postprocess' => 'tabs_reload+update_session_titre'));
            echo get_input_jpost($this, "specialite", array('select' => '', 'label' => __("Spécialité"), 'first' => __("Choisissez une spécialité, la plus précise possible")));
        ?>
        </fieldset>
        
        <fieldset>
            <legend><?php _e("Équipe pédagogique"); ?></legend>
        <?php
            if (!isset($wpof->formateur))
                init_term_list("formateur");
            echo get_input_jpost($this, "formateur", array('select' => 'multiple', 'rows' => 10, 'postprocess' => 'check_my_formation'));
        ?>
        </fieldset>
        
        <fieldset>
            <legend><?php _e("Stagiaires"); ?></legend>
            <?php echo get_input_jpost($this, "stagiaires_min", array('input' => 'number', 'step' => 1, 'min' => 1, 'label' => __('Nombre minimum souhaité'))); ?>
            <?php echo get_input_jpost($this, "stagiaires_max", array('input' => 'number', 'step' => 1, 'min' => 1, 'label' => __('Nombre maximum souhaité'))); ?>
            <?php echo get_input_jpost($this, "tarif_total_chiffre", array('input' => 'number', 'step' => 0.01, 'min' => 0, 'label' => __('Tarif/stagiaire affiché'), 'after' => $wpof->monnaie_symbole, 'postprocess' => 'update_pour_infos_session')); ?>
        </fieldset>
        
        <div class="pour-infos">
            <?php echo $this->get_pour_infos_box(); ?>
        </div>
        
        </div>
        <fieldset class="edit-data">
            <legend><?php _e("Documents administratifs pour la session"); ?></legend>
            <?php echo get_gestion_docs($this); ?>
        </fieldset>
            <?php echo $this->get_televersement_fieldset(); ?>
        </div> <!-- tab-session -->
        
        <div id="tab-dates">
            <?php echo get_icone_aide("tab_dates", __("Dates et créneaux")); ?>
            <h3><?php _e("Dates et créneaux"); ?></h3>
            <p>
            <?php
                _e("Types de créneaux : ");
                foreach($wpof->type_creneau as $type => $text)
                    echo "<span class='faux-bouton creneau $type'>$text</span>";
            ?>
            </p>
            <?php echo $this->get_html_creneaux(true); ?>
            
            <p class="decale_date">
            <?php if (count($this->creneaux) > 1) : // on affiche la fonction de décalage uniquement si deux dates ou plus sont définies ?>
            <?php echo get_icone_aide("decale_date"); ?>
            <?php _e("Décaler la première date au "); ?>
            <input class="datepicker" name="decale_date" type="text" value="" />
            <span class="icone-bouton valider_decale_date"><?php _e("Valider"); ?></span>
            <?php endif; ?>
            </p>
            
            <?php if (champ_additionnel('duree_jour')) : ?>
            <div class="edit-data">
            <?php echo get_input_jpost($this, "nb_jour", array('input' => 'text', 'label' => __("Indication de durée en jours ou demies-journées"))); ?>
            </div>
            <?php endif; ?>
            
        </div> <!-- tab-dates -->
        
        <div id="tab-lieu">
            <?php echo get_icone_aide("tab_lieu", __("Informations sur le lieu")); ?>
            <h3><?php _e("Lieu"); ?></h3>
        <div class="lieu-session editable">
            <div class="select edit-data">
            <?php
            init_term_list("lieu");
            echo get_input_jpost($this, "lieu", array('select' => '', 'label' => __("Lieu prédéfini"), 'first' => __("Lieu occasionnel"), 'postprocess' => 'toggle_lieu_details+tabs_reload+update_session_titre'));
            ?>
            </div>
            <?php echo $this->get_lieu_details(); ?>
        </div>
        </div> <!-- tag-lieu -->
        
        <div id="tab-formation" class="desc-session edit-data metadata notif-modif">
            <?php echo get_icone_aide("tab_formation", __("Décrire la session")); ?>
            <h2><?php _e("Description de la formation pour cette session"); ?></h2>
            <?php
                foreach($wpof->desc_formation->term as $k => $t)
                {
                    if ($wpof->{"formation_".$k."_mode"} != 'force')
                        echo get_input_jpost($this, $k, array('editor' => '', 'label' => $t->text));
                    else
                    {
                        ?>
                        <h3><?php echo $t->text; ?></h3>
                        <p><em><?php _e("Ce paramètre est fixé par votre responsable de formation et ne peut être modifié."); ?></em></p>
                        <p><?php echo $wpof->{"formation_".$k."_text"}; ?></p>
                        <?php
                    }
                }
            ?>
            <p class="bouton submit enregistrer-formation-input"><?php _e("Enregistrer les informations"); ?></p>
        </div> <!-- tab-formation -->
        
        <div id="tab-client">
            <p class="icone-bouton dynamic-dialog" data-function="new_client" data-sessionid="<?php echo $this->id; ?>"><span class="dashicons dashicons-plus-alt" > </span> <?php _e("Ajouter client"); ?></p>
            <?php echo get_icone_aide("tab_clients", __("Gestion des clients")); ?>
            
            <div id="tabs-clients">
            <?php echo $this->get_tabs_clients(); ?>
            </div>
        </div> <!-- tab-client -->
        
        <div id="tab-eval">
            <?php echo get_icone_aide("tab_eval", __("Gestion des évaluations")); ?>
            <?php echo $this->get_evaluations_tab(); ?>
        </div> <!-- tab-eval -->
        
        <div id="tab-token">
            <?php echo get_icone_aide("tab_token", __("Gestion des accès privés")); ?>
            <?php echo $this->get_token_tab(); ?>
        </div> <!-- tab-eval -->
        
        </div> <!-- session-formation -->
        
        <?php
    }
    
    public function get_pour_infos_box()
    {
        global $wpof;
        $this->calcule_temps_session();
        $this->calcule_tarif();
        $this->calcule_budget_global();
       
        ob_start();
        ?>
            <h3><?php _e("Pour information"); ?></h3>
            <p><?php _e("Dates concernées"); ?> : <span class="dates_concernees"><?php echo $this->dates_texte; ?></span></p> 
            <p><?php _e("Tarif horaire par stagiaire (affiché)"); ?> : <span class="tarif_heure"><?php echo get_tarif_formation($this->tarif_heure); ?></span> </p>
            <p><?php _e("Nombre d'heures"); ?> : <span class="nb_heure"><?php echo $this->nb_heure; ?></span> </p>
            <p><?php _e("Tarif total par stagiaire (affiché)"); ?> : <span class="tarif_total"><?php echo get_tarif_formation($this->tarif_total_chiffre); ?></span> </p>
            <p><?php _e("Budget global"); ?> : <span class="budget_global"><?php echo get_tarif_formation($this->budget_global); ?> </span> </p>
        <?php
        return ob_get_clean();
    }
    
    public function get_lieu_details()
    {
        global $wpof;
        ob_start();
        ?>
            <div class="lieu details <?php if ($this->lieu == -1) echo "edit-data"; ?>">
            <?php
            foreach($wpof->desc_lieu->term as $key => $term)
            {
                switch ($term->type)
                {
                    case "text":
                    case "number":
                        echo get_input_jpost($this, "lieu_$key", array('input' => $key, 'label' => $term->text));
                        break;
                    case "textarea":
                    case "editor":
                    case "select":
                        echo get_input_jpost($this, "lieu_$key", array($term->type => '', 'label' => $term->text));
                        break;
                    default:
                        echo "<p>$key pas encore géré</p>";
                        break;
                }
                
            }
            ?>
            </div>
        <?php
        
        return ob_get_clean();
    }
    
    public function get_evaluations_tab()
    {
        $role = wpof_get_role(get_current_user_id());
        $html = "";
        $quizpr = new Quiz($this->quizpr_id);
        if (empty($quizpr->sujet))
            $quizpr->set_identite("prerequis", $this->id);
        $html .= $quizpr->get_edit_questions();
        $html .= ($role == 'admin') ? "<div>quizpr_id = ".$this->quizpr_id."</div>" : "";
        
        $quizobj = new Quiz($this->quizobj_id);
        if (empty($quizobj->sujet))
            $quizobj->set_identite("objectifs", $this->id);
        $html .= $quizobj->get_edit_questions();
        $html .= ($role == 'admin') ? "<div>quizobj_id = ".$this->quizobj_id."</div>" : "";
        
        return $html;
    }
    
    
    public function get_token_tab()
    {
        $token_list = array();
        
        foreach($this->clients as $c)
        {
            $client = get_client_by_id($this->id, $c);
            
            if ($client->token != "")
            {
                $token_info = new stdClass();
                $token_info->id = $c;
                $token_info->nom = $client->nom;
                $token_info->class = "Client";
                $token_info->email = implode(', ', array($client->contact_email, $client->responsable_email));
                $token_info->token = $client->token;
                $token_info->token_time = $client->token_time;
                $token_info->valid_token = valid_token($client->token_time);
                $token_info->acces = $client->acces;
                $token_list[] = $token_info;
            }
            
            if (!empty($client->stagiaires))
                foreach($client->stagiaires as $stagiaire_id)
                {
                    $stagiaire = get_stagiaire_by_id($this->id, $stagiaire_id);
                    
                    if ($stagiaire->token != "")
                    {
                        $token_info = new stdClass();
                        $token_info->id = $stagiaire_id;
                        $token_info->nom = $stagiaire->get_displayname();
                        $token_info->class = "Stagiaire";
                        $token_info->email = $stagiaire->email;
                        $token_info->token = $stagiaire->token;
                        $token_info->token_time = $stagiaire->token_time;
                        $token_info->valid_token = valid_token($stagiaire->token_time);
                        $token_info->acces = $stagiaire->acces;
                        $token_list[] = $token_info;
                    }
                }
        }
        
        if (!empty($token_list))
        {
            global $wpof;
            
            ob_start();
            ?>
            <table class="token_list opaga">
            <thead>
            <tr>
            <th class="thin"><?php _e('Entité'); ?></th>
            <th><?php _e('Nom/raison sociale'); ?></th>
            <th><?php _e('Email'); echo get_icone_aide("token_email"); ?></th>
            <th><?php _e('Lien privé'); echo get_icone_aide("token_lien"); ?></th>
            <th><?php _e('Validité'); echo get_icone_aide("token_validite"); ?></th>
            <th><?php _e('Dernier accès'); echo get_icone_aide("token_dernier_acces"); ?></th>
            <th class="thin"><?php _e('Supprimer'); ?></th>
            <th class="thin"><?php _e('Renouveler'); echo get_icone_aide("token_renouveler"); ?></th>
            </tr>
            </thead>
            
            <tbody>
            <?php foreach($token_list as $token_info) : ?>
            <tr class="<?php echo $token_info->class; ?> token_info" data-id="<?php echo $token_info->id; ?>">
            <td><?php echo $token_info->class; ?></td>
            <td><?php echo $token_info->nom; if (debug) echo " (".$token_info->id.")"; ?></td>
            <td><?php echo $token_info->email; ?></td>
            <td><a href="<?php echo get_site_url()."/".$wpof->url_acces."/?t=".$token_info->token; ?>"><?php echo $token_info->token; ?></td>
            <td>
                <?php
                    if ($token_info->valid_token === true)
                        echo ($token_info->acces > 0) ? __("Illimitée") : __("Non utilisé");
                    else
                        echo ($token_info->valid_token === false) ? __("Expiré") : get_pretty_duree($token_info->valid_token); ?>
            </td>
            <td><?php if ($token_info->acces > 0) echo date_i18n("d/m/Y à H:i", $token_info->acces); ?></td>
            <td class="center">
                <p class="icone-bouton token-delete" data-id="<?php echo $token_info->id; ?>" data-remove="tr.token_info" data-class="<?php echo $token_info->class; ?>"><span class="dashicons dashicons-dismiss"></span></p>
            </td>
            <td class="center">
                <p class="token-reset icone-bouton" data-email="<?php echo $token_info->email; ?>"><span class="dashicons dashicons-update-alt"></span></p>
            </td>
            </tr>
            <?php endforeach; ?>
            </tbody>
            </table>
            <?php
            
            return ob_get_clean();
        }
        
        return "";
    }
    
    /*
     * Retourne l'onglet des clients
     */
    public function get_tabs_clients()
    {
        $list_tabs = "";
        $content_tabs = "";
        
        foreach($this->clients as $c)
        {
            $client = new Client($this->id, $c);
            $client->init_stagiaires();
            if ($client->entite_client == "morale")
                $nom = (empty($client->nom)) ? __("Sans nom") : $client->nom;
            else
                $nom = (empty($client->stagiaire->nom)) ? __("Sans nom") : $client->stagiaire->prenom." ".$client->stagiaire->nom;
            $list_tabs .= "<li class='tab-client' data-id='$c'><a href='#tab-c$c'>".$nom."</a></li>";
            $content_tabs .= $client->get_the_board();
            
            if ($client->entite_client == "morale" && !empty($client->stagiaires))
                foreach($client->stagiaires as $stagiaire_id)
                {
                    $stagiaire = get_stagiaire_by_id($this->id, $stagiaire_id);
                    $list_tabs .= "<li class='tab-stagiaire' data-id='$stagiaire_id'><a href='#tab-s$stagiaire_id'>".$stagiaire->get_displayname()."</a></li>";
                    $content_tabs .= $stagiaire->get_the_board();
                }
        }
        return "<ul>$list_tabs</ul>\n".$content_tabs;
    }
    
    
    /*
     * Retourne le découpage temporel de la session
     */
    public function get_decoupage_temporel($liste_creneaux = array())
    {
        global $wpof;
        ob_start();
        if (empty($liste_creneaux))
        {
            $creneaux = $this->creneaux;
        }
        else
        {
            $creneaux = array();
            foreach($this->creneaux as $date => $tab_creno)
                foreach($tab_creno as $cid => $creno)
                    if ($liste_creneaux[$cid] > 0)
                        $creneaux[$date][$cid] = $creno;
        }
        
        ?>
        <dl class="decoupage_temporal">
        <?php foreach($creneaux as $date => $tab_creno) : ?>
        <dt><?php echo pretty_print_dates($date); ?></dt>
            <?php foreach($tab_creno as $c) : ?>
            <dd>
            <?php
                echo $c->titre." — ".$wpof->type_creneau[$c->type];
                $types[$c->type] = 1;
            ?>
            </dd>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <div class="creneau_legende">
        <?php
            $legende = array();
            if (isset($types['presentiel'])) $legende[] = "<span class='intitule'>".$wpof->type_creneau['presentiel']."</span>, ".__("en salle de formation ou chez le client");
            if (isset($types['foad_sync'])) $legende[] = "<span class='intitule'>".$wpof->type_creneau['foad_sync']."</span>, ".__("en visio-conférence");
            if (isset($types['foad_async'])) $legende[] = "<span class='intitule'>".$wpof->type_creneau['foad_async']."</span>, ".__("en autonomie, avec disponibilité de l'équipe pédagogique");
            if (isset($types['afest'])) $legende[] = "<span class='intitule'>".$wpof->type_creneau['afest']."</span>, ".__("formation en situation de travail");
            if (isset($types['technique'])) $legende[] = "<span class='intitule'>".$wpof->type_creneau['technique']."</span>, ".__("accompagnement technique, informel sur le déroulé de la session");
            
            echo join(" — ", $legende);
        ?>
        </div>
        <?php
        return ob_get_clean();
    }
    
    protected function get_televersement_fieldset()
    {
        ?>
            <fieldset><legend><?php _e("Téléversement"); ?></legend>
            <p><?php _e("Déposez ici les documents signés et scannés ou tout autre document qui vous parait utile (devis, facture par exemple)"); ?></p>
            <p><?php echo __("Poids maximal par fichier : ").ini_get("upload_max_filesize"); ?></p>
            
            <form method="POST" name="upload" enctype="multipart/form-data">
                <input name="scan" type="file" multiple />
                <?php
                    echo hidden_input('id_span_message', 'archive-message');
                    echo hidden_input('action', 'archive_file');
                    echo hidden_input('session_id', $this->id);
                ?>
                <input class="ajax-save-file bouton" type="button" value="<?php _e("Déposer scan") ?>" />
            </form>
            <p id="archive-message" class="message"></p>
            <table class='gestion-docs-admin' id="liste-scan">
            <tr>
            <th><?php _e("Fichier"); ?></th>
            <th><?php _e("Date de dépôt"); ?></th>
            <!-- <th><?php //_e("Diffuser à"); ?></th> -->
            <th><?php _e("Supprimer"); ?></th>
            </tr>
            <?php
            foreach ($this->uploads as $u)
                echo $u->get_html('tr');
            ?>
            </table>
            
            </fieldset>
            
        <?php
    }

    /*
     * Renvoie la liste de tous les créneaux classés par dates sous forme html
     * $edit : si true on peut modifier les créneaux, si false, ils sont simplement affichés
     * $actif : tableau $session_stagiaire->creneaux contenant la liste des ID des créneaux auxquels un stagiaire est inscrit
     */
    public function get_html_creneaux($edit = false, $objet = null)
    {
        $actif = $data_objet = "";
        if ($objet)
        {
            $objet_class = get_class($objet);
            if (in_array($objet_class, array('Client', 'SessionStagiaire')))
            {
                $actif = $objet->creneaux;
                if ($objet_class == "SessionStagiaire")
                    $data_objet = "data-objetid='{$objet->user_id}' data-objet='SessionStagiaire'";
                else
                    $data_objet = "data-objetid='{$objet->id}' data-objet='$objet_class'";
            }
        }
        ob_start();
        
        if (!$edit && count($this->creneaux) == 0)
            echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
        else
        {
            ?>
            <div class='tableau-creneau' <?php echo $data_objet; ?> data-sessionid='<?php echo $this->id; ?>'>
            <?php
            /*
            if (count($this->creneaux) == 0)
                echo $this->get_html_ligne_creneaux($edit);
            else
            {*/
                foreach($this->creneaux as $date => $creneau)
                    echo $this->get_html_ligne_creneaux($edit, $date, $actif);
            //}
            if ($edit)
            {
                ?>
                    <div class="icone-bouton add-date empty-date"><span class="dashicons dashicons-plus-alt"></span> <?php _e("Date"); ?>
                    </div>
                    <?php echo get_icone_aide("creneau_ajouter_date"); ?>
                <?php
            }
            echo "</div>";
        }
        
        return ob_get_clean();
    }
    
    /*
     * Renvoie une ligne de créneaux correspondant à une date au format DateTime
     */
    public function get_html_ligne_creneaux($edit = false, $date = null, $actif = null)
    {
        $weekday = get_week_day($date);
        
        ob_start();
        echo "<div class='liste-creneau' data-date='$date'>";
        
        if ($date == null)
        {
            if ($edit)
                echo '<span class="dashicons dashicons-dismiss icone del-date"></span><input class="datepicker" name="dates[]" type="text" value="" />';
        }
        else
        {
            if ($edit)
            {
                echo get_icone_aide("creneau_supprimer_date");
                echo '<span class="dashicons dashicons-dismiss icone del-date"></span>';
                echo '<input class="datepicker" name="dates[]" type="text" value="'.$date.'" />';
            }
            else
                echo "<span class='creneau-date'>$date</span>";
                
            foreach ($this->creneaux[$date] as $creno)
            {
                $creno_actif = (is_array($actif) && isset($actif[$creno->id]) && $actif[$creno->id] == 1) ? "actif" : "";
                echo $creno->get_html($creno_actif, $edit);
            }
        }
        
        if ($edit)
        {
            ?>
            <div class="icone-bouton dynamic-dialog" data-function="add_or_edit_creneau" data-crenoid="-1" data-sessionid="<?php echo $this->id; ?>">
                <span class="dashicons dashicons-plus-alt"></span>
                <?php _e("Créneau"); ?>
            </div>
            <div class="icone-bouton add-date" data-decaljour="1"><span class="dashicons dashicons-plus-alt"></span> <?php _e("Copie +1 j"); ?></div>
            <div class="icone-bouton add-date" data-decaljour="7"><span class="dashicons dashicons-plus-alt"></span> <?php _e("Copie +7 j"); ?></div>
            <?php echo get_icone_aide("creneau_boutons_edit"); ?>
            <p class="float right">
            <?php echo get_week_day($date, true); ?>
            </p>
            <?php
        }
        else
            echo get_icone_aide("creneau_active");
        echo "</div>";
        
        return ob_get_clean();
    }
    
    
    public function get_select_client($selected_client_id)
    {
        ob_start();
        ?>
        <select name="change_client">
        <?php foreach($this->clients as $client_id) : ?>
            <?php $client = get_client_by_id($this->id, $client_id); ?>
            <option value="<?php echo $session_id; ?>" <?php selected($selected_client_id, $client_id); ?>><?php echo $client->get_nom(); ?></option>
        <?php endforeach; ?>
        </select>
        <?php
        return ob_get_clean();
    }
    
    public function get_sql_select_button($stagiaire_id = -1)
    {
        return "<span class='sql_select bouton' data-sessionid='".$this->id."' data-stagiaireid='$stagiaire_id'>SQL</span>";
    }
    
    public function get_doc_titre_formation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Intitulé de la formation"));
        return $result[$arg];
    }
    public function get_doc_formateur($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->get_formateurs_noms(), 'tag' => "session:$function_name", 'desc' => __("Équipe pédagogique"));
        return $result[$arg];
    }
    public function get_doc_duree($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->nb_heure_decimal." h", 'tag' => "session:$function_name", 'desc' => __("Durée en heures"));
        return $result[$arg];
    }
    public function get_doc_nb_jour($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name." h", 'tag' => "session:$function_name", 'desc' => __("Durée indicative personnalisée (par ex. en journées ou demies-journées)"));
        return $result[$arg];
    }
    public function get_doc_tarif_heure($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name." ".$wpof->monnaie_symbole, 'tag' => "session:$function_name", 'desc' => __("Tarif public horaire (inter)"));
        return $result[$arg];
    }
    public function get_doc_tarif_total_chiffre($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name." ".$wpof->monnaie_symbole, 'tag' => "session:$function_name", 'desc' => __("Tarif total public en chiffres"));
        return $result[$arg];
    }
    public function get_doc_tarif_total_lettre($arg = 'valeur')
    {
        global $wpof;
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name." ".$wpof->monnaie."s", 'tag' => "session:$function_name", 'desc' => __("Tarif total public en lettres"));
        return $result[$arg];
    }
    public function get_doc_dates($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->dates_texte, 'tag' => "session:$function_name", 'desc' => __("Toutes les dates de la session"));
        return $result[$arg];
    }
    public function get_doc_premiere_date($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $date = "";
        if ($arg == 'valeur' && $this->first_date != "")
            $date = pretty_print_dates($this->first_date);
        
        $result = array('valeur' => $date, 'tag' => "session:$function_name", 'desc' => __("Première date de la session"));
        return $result[$arg];
    }
    public function get_doc_derniere_date($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $date = "";
        if ($arg == 'valeur' && !empty($this->creneaux))
            $date = pretty_print_dates(end(array_keys($this->creneaux)));
        
        $result = array('valeur' => $date, 'tag' => "session:$function_name", 'desc' => __("Dernière date de la session"));
        return $result[$arg];
    }
    public function get_doc_lieu_nom($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Nom du lieu d’animation de la session"));
        return $result[$arg];
    }
    public function get_doc_lieu_adresse($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Adresse du lieu d’animation de la session"));
        return $result[$arg];
    }
    public function get_doc_lieu_code_postal($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Code postal du lieu d’animation de la session"));
        return $result[$arg];
    }
    public function get_doc_lieu_ville($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Ville du lieu d’animation de la session"));
        return $result[$arg];
    }
    public function get_doc_lieu_localisation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Informations de localisation du lieu d’animation de la session"));
        return $result[$arg];
    }
    public function get_doc_stagiaires_min($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Nombre minimum de stagiaires pour réaliser la session"));
        return $result[$arg];
    }
    public function get_doc_stagiaires_max($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Nombre maximum de stagiaires acceptés dans la session"));
        return $result[$arg];
    }
    public function get_doc_nb_stagiaires($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Nombre de stagiaires intéressés"));
        return $result[$arg];
    }
    public function get_doc_nb_confirmes($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Nombre de stagiaires confirmés"));
        return $result[$arg];
    }
    public function get_doc_date_modif($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "session:$function_name", 'desc' => __("Date de dernière modification du programme"));
        return $result[$arg];
    }
    public function get_doc_creneaux($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->get_decoupage_temporel(), 'tag' => "session:$function_name", 'desc' => __("Planning de la session"));
        return $result[$arg];
    }
    public function get_doc_presentation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_objectifs_pro($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_objectifs($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_prerequis($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_public_cible($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_modalites_pedagogiques($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_ressources($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_organisation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_materiel_pedagogique($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_accessibilite($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_modalites_evaluation($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_inscription_delai($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_programme($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
    public function get_doc_propriete_intellectuelle($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $desc = "";
        if ($arg == 'desc')
        {
            global $wpof;
            $desc = $wpof->desc_formation->get_term($function_name);
        }
        $result = array('valeur' => wpautop($this->$function_name), 'tag' => "session:$function_name", 'desc' => $desc);
        return $result[$arg];
    }
}

function get_session_by_id($id)
{
    global $SessionFormation;
    log_add(__FUNCTION__."($id)", "session");
    
    if (!isset($SessionFormation[$id]))
    {
        $SessionFormation[$id] = new SessionFormation($id);
    }
    return $SessionFormation[$id];
}

