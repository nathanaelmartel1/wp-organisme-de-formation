<?php
/*
 * class-document.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

// Inclure dompdf, HtmlDomParser installés via composer
require_once wpof_path . '/vendor/autoload.php';
use Dompdf\Dompdf;
use Dompdf\Options;
use voku\helper\HtmlDomParser;

define( 'wpof_url_pdf', WP_CONTENT_URL . "/uploads/pdf");
define( 'wpof_path_pdf', WP_CONTENT_DIR . "/uploads/pdf");

class Document
{
    /* Valeurs pour l'état de validation (signature) de chaque document
     * NEED : le document devra être signé
     * REQUEST : un message est envoyé pour que la personne signe ce document
     * DONE : le document est signé
     * Attention : REQUEST doit disparaître lorsque DONE apparaît !
     */
    const VALID_STAGIAIRE_NEED = 1;
    const VALID_STAGIAIRE_REQUEST = 2;
    const VALID_STAGIAIRE_DONE = 4;
    const VALID_CLIENT_NEED = 8;
    const VALID_CLIENT_REQUEST = 16;
    const VALID_CLIENT_DONE = 32;
    const VALID_RESPONSABLE_NEED = 64;
    const VALID_RESPONSABLE_REQUEST = 128;
    const VALID_RESPONSABLE_DONE = 256;
    const VALID_FORMATEUR_NEED = 2048;
    const VALID_FORMATEUR_REQUEST = 4096;
    const VALID_FORMATEUR_DONE = 8192;
    const VALID_ALL_NEED = Document::VALID_CLIENT_NEED | Document::VALID_STAGIAIRE_NEED | Document::VALID_RESPONSABLE_NEED | Document::VALID_FORMATEUR_NEED;
    const VALID_ALL_REQUEST = Document::VALID_CLIENT_REQUEST | Document::VALID_STAGIAIRE_REQUEST | Document::VALID_RESPONSABLE_REQUEST | Document::VALID_FORMATEUR_REQUEST;
    const VALID_ALL_DONE = Document::VALID_CLIENT_DONE | Document::VALID_STAGIAIRE_DONE | Document::VALID_RESPONSABLE_DONE | Document::VALID_FORMATEUR_DONE;
    const DRAFT = 512;
    const SCAN = 1024;
    
    // qui doit signer et est-ce que le document est signé (utilise les constantes ci-dessus)
    public $valid = 0;
    
    
    /* Valeurs pour les colonnes du tableau gestion-docs-admin
    */
    const COL_ALL = 0xFFFF;
    const COL_DOCUMENT = 1;
    const COL_DRAFT = 2;
    const COL_FINAL = 4;
    const COL_REQUEST = 8;
    const COL_DIFFUSER = 16;
    const COL_SCAN = 32;
    const COL_SUPPRIMER = 64;
    const COL_NOM_ENTITE = 128;
    const COL_REJETER = 256;

    // type de document : convention, ri, attestation de suivi, feuille d'émargement, etc.
    public $type = "";
    
    // chemin et nom de fichiers
    public $path = wpof_path_pdf;
    public $url = wpof_url_pdf;
    public $base_filename = "";
    public $html_filename = "";
    public $pdf_filename = "";
    public $text_name = "";
    public $link_name = "";
    
    // le document est-il diffusé au stagiaire
    public $visible = false;
    
    // date de dernière modification
    public $last_modif = 0;
    
    // est-ce que le document a une plage de validité ?
    // si false, on utilise la date du jour
    public $date_debut = "";
    public $date_fin = "";
    
    // entité concernée
    public $stagiaire_id = null;
    public $client_id = null;
    public $array_formateur_id = array();
    
    // la personne qui crée le document, peut-elle le signer ?
    public $signataire = false;
    
    // session de formation concernée
    public $session_formation_id = null;
    
    // modèle
    public $modele;
    // le modèle de ce document a combien de mots-clés personnalisés (Custom KeyWords) ?
    public $ckw = 0;
    public $ckw_ok = true; // les mots-clés sont-ils définis pour ce document ?
    
    public $titre = "";
    
    public $infos_manquantes = array();
    
    // table suffix
    private $table_suffix = WPOF_TABLE_SUFFIX_DOCUMENTS;
    
    // objet DomPDF
    private $dompdf = null;
    private $pdf_opt = array();
    
    // l'id d'un document est de la forme : type-<session_id> ou type-[csf]<contexte_id>
    // c → client, s → stagiaire, f → formateur
    public $id;
    
    public function __construct($type = null, $session_formation_id = -1, $contexte = 0, $contexte_id = -1)
    {
        global $wpdb, $wpof, $Client, $SessionStagiaire;
        
        if ($type)
        {
            $table_documents = $wpdb->prefix.$this->table_suffix;
            
            $this->session_formation_id = $session_formation_id;
            $this->type = $type;
            $this->contexte = $contexte;
            $this->contexte_id = ($contexte_id == -1) ? $session_formation_id : $contexte_id;
            $this->text_name = $wpof->documents->get_term($type);
            $this->path .= "/$session_formation_id"; // chaque session a son dossier (TODO : qui pourrait être créé dès la création de la session, voir dans cpt-session-formation)
            $this->url .= "/$session_formation_id";
            $this->base_filename = "$type";
            $this->modele = get_modele_by_slug($this->type);
            if ($this->modele->id > 0)
            {
                $this->titre = $this->modele->titre;
                $this->ckw = count($this->modele->ckw);
            }
            elseif ($wpof->documents->is_term($this->type))
                $this->titre = $wpof->documents->get_term($this->type);
            else
                $this->titre = "pas de titre";
            
            
            switch ($contexte & $wpof->doc_context->entite)
            {
                case $wpof->doc_context->session:
                    $this->id = "$type-{$this->contexte_id}";
                    break;
                case $wpof->doc_context->client:
                    $this->id = "$type-c{$this->contexte_id}";
                    $this->client_id = $this->contexte_id;
                    $client = get_client_by_id($this->session_formation_id, $this->client_id);
                    $this->base_filename .= "-".sanitize_title($client->nom);
                    if ($client->entite_client == "physique")
                        $this->stagiaire_id = $client->stagiaires[0];
                    break;
                case $wpof->doc_context->stagiaire:
                    $this->id = "$type-s{$this->contexte_id}";
                    $this->stagiaire_id = $this->contexte_id;
                    $stagiaire = get_stagiaire_by_id($this->session_formation_id, $this->stagiaire_id);
                    $this->client_id = $stagiaire->client_id;
                    $this->base_filename .= "-".sanitize_title($stagiaire->get_displayname());
                    break;
                case $wpof->doc_context->formateur:
                    $this->id = "$type-f{$this->contexte_id}";
                    $this->array_formateur_id[0] = $this->contexte_id;
                    break;
                case $wpof->doc_context->formation:
                    $this->id = "$type-f{$this->contexte_id}";
                    $this->formation_id = $this->contexte_id;
                    break;
            }
            if ($session_formation_id > 0)
            {
                $this->session = get_session_by_id($session_formation_id);
                $this->array_formateur_id = $this->session->formateur;
            }
            elseif ($contexte & $wpof->doc_context->formation)
            {
                $this->formation = get_formation_by_id($this->contexte_id);
                $this->array_formateur_id = $this->formation->formateur;
            }
            
            $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_documents WHERE session_id = '%d' AND contexte & '%d' AND contexte_id = '%d' AND document = '%s';",
                $this->session_formation_id,
                $this->contexte,
                $this->contexte_id,
                $this->type
                );
            $res = $wpdb->get_results($query, OBJECT);
            
            foreach($res as $row)
            {
                if (isset($this->{$row->meta_key}) && is_array($this->{$row->meta_key}))
                    $this->{$row->meta_key} = unserialize(stripslashes($row->meta_value));
                else
                    $this->{$row->meta_key} = stripslashes($row->meta_value);
            }
            
            if ($this->ckw > 0)
                $this->set_ckw_ok();
            
            $this->valid |= (integer) $wpof->documents->term[$this->type]->signature;
            
            // si l'utilisateur n'est pas connecté on ne fait pas de brouillon (retrait du bit DRAFT)
            if (!is_user_logged_in())
                $this->valid &= ~ Document::DRAFT;
            
            $this->path .= ($this->valid & Document::SCAN) ? "/scan" : "";
            $this->url .= ($this->valid & Document::SCAN) ? "/scan" : "";
            
            $this->init_link_name();
        }
    }
    
    public function init_link_name()
    {
        if ($this->pdf_filename != "")
        {
            $this->href = home_url()."?download={$this->type}&s={$this->session_formation_id}&ci={$this->contexte_id}&c={$this->contexte}";
            if (isset($_GET['t']))
                $this->href .= "&t=".$_GET['t'];
            $this->link_name = "<a href='{$this->href}'>".$this->text_name."</a>";
        }
        else
            $this->link_name = $this->text_name;
    }
    
    public function get_meta($meta_key)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("SELECT meta_value from $table
            WHERE contexte_id = '%d'
            AND contexte = '%d'
            AND session_id = '%d'
            AND document = '%s'
            AND meta_key = '%s';",
            $this->contexte_id, $this->contexte, $this->session_formation_id, $this->type, $meta_key);
        
        return $wpdb->get_var($query);
    }
    
    public function update_meta($meta_key, $meta_value = null)
    {
        // mise à jour de l'instance courante
        if ($meta_value === null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        if (is_array($meta_value))
            $meta_value = serialize($meta_value);

        // mise à jour de la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("INSERT INTO $table (session_id, contexte, contexte_id, document, meta_key, meta_value)
            VALUES ('%d', '%d', '%d', '%s', '%s', '%s')
            ON DUPLICATE KEY UPDATE meta_value = '%s';",
            $this->session_formation_id, $this->contexte, $this->contexte_id, $this->type, $meta_key, $meta_value, $meta_value);
        return $wpdb->query($query);
    }
    
    
    // Fixe la propriété ckw_ok a la bonne valeur
    public function set_ckw_ok()
    {
        $this->ckw_ok = true;
        foreach($this->modele->ckw as $key => $ckw)
            if ($ckw->needed == 1 && (!isset($this->$key) || $this->$key == ""))
            {
                $this->ckw_ok = false;
                break;
            }
    }
    
    public function get_html_ligne($cols = Document::COL_ALL & ~ (Document::COL_NOM_ENTITE | Document::COL_REJETER))
    {
        global $wpof;
        
        $role = wpof_get_role(get_current_user_id());
        $formation_id = get_post_meta("formation", $this->session_formation_id, true);
        
        // si l'utilisateur est signataire, on n'affiche pas la colonne de demande de signature
        if ($this->signataire)
            $cols &= ~ Document::COL_REQUEST;
        
        $html = "<tr id='tr-{$this->id}' class='docrow' data-cols='$cols' data-sessionid='{$this->session_formation_id}' data-contexteid='{$this->contexte_id}' data-contexte='{$this->contexte}' data-typedoc='{$this->type}' data-docuid='{$this->id}'>";
        
        // Nom du document
        if ($cols & Document::COL_DOCUMENT)
        {
            $icones = "";
            $brouillon = "";
            $periode_validite = "";
            $complet = "";
            
            if ($this->valid & Document::DRAFT)
                $brouillon = " <span class='alerte'>(".__("Brouillon").")</span> ";
            
            $entite_nom = "";
            if ($cols & Document::COL_NOM_ENTITE)
            {
                if ($this->stagiaire_id > 0)
                {
                    $stagiaire = get_stagiaire_by_id($this->session_formation_id, $this->stagiaire_id);
                    $entite_nom = " – ".$stagiaire->get_displayname()." (".__("stagiaire").")";
                }
                elseif ($this->client_id > 0)
                    $entite_nom = " – ".get_client_meta($this->client_id, "nom")." (".__("client").")";
                elseif (!empty($this->array_formateur_id))
                    $entite_nom = " – ".get_displayname($this->array_formateur_id[0])." (".__("formateur externe").")";
            }
                    
            $infos_doc = "";
            if (!empty($this->last_modif))
            {
                $infos_doc = "<p class='infos_doc'><span class='last-modif'>{$wpof->doc_last_modif} : ".date_i18n("j/m/Y H:i:s", $this->last_modif)."</span>";
                $signatures = array();
                if ($this->valid & Document::VALID_RESPONSABLE_NEED)
                {
                    if ($this->valid & Document::VALID_RESPONSABLE_DONE)
                        $signatures['resp'] = "<span class='signature fait'>";
                    else
                        $signatures['resp'] = "<span class='signature alerte'>";
                    $signatures['resp'] .= __("Responsable")."</span>";
                    
                }
                if ($this->valid & Document::VALID_CLIENT_NEED)
                {
                    if ($this->valid & Document::VALID_CLIENT_DONE)
                        $signatures['client'] = "<span class='signature fait'>";
                    else
                        $signatures['client'] = "<span class='signature alerte'>";
                    $signatures['client'] .= __("Client")."</span>";
                }
                if ($this->valid & Document::VALID_STAGIAIRE_NEED)
                {
                    if ($this->valid & Document::VALID_STAGIAIRE_DONE)
                        $signatures['stag'] = "<span class='signature fait'>";
                    else
                        $signatures['stag'] = "<span class='signature alerte'>";
                    $signatures['stag'] .= __("Stagiaire(s)")."</span>";
                }
                if (count($signatures) > 0)
                    $infos_doc .= " — ".__("Document signé par : ").join(" / ", $signatures);
                $infos_doc .= "</p>";
                
                if (!($this->valid & Document::SCAN) && $this->ckw > 0)
                {
                    $etat = ($this->ckw_ok) ? "succes" : "alerte";
                    $infobulle = ($this->ckw_ok) ? __("Tous les champs personnalisés sont renseignés, mais vous pouvez les modifier") : __("Il reste des champs personnalisés à renseigner");
                    $icones .= "<span class='right float doc-check-ckw icone-doc big dashicons dashicons-edit $etat' title='$infobulle'></span>";
                }
                
                $icones .= "<a class='right float icone' href='{$this->href}'><span class='big dashicons dashicons-download'></span></a>";
                
                $complet = (empty($this->infos_manquantes))
                    ? " <span class='pastille bg-succes dashicons dashicons-saved' title='".__("Document complet")."'></span>"
                    : " <span class='pastille bg-erreur infos-manquantes' title='".sprintf(__("%d informations manquantes"), count($this->infos_manquantes))."'>".count($this->infos_manquantes)."</span>";
                
            }
            
            if (champ_additionnel('periode_validite') && $this->valid & Document::VALID_RESPONSABLE_NEED)
            {
                $validite_input_args = array("input" => "datepicker", "inline" => 1);
                if ($this->valid & Document::VALID_RESPONSABLE_DONE)
                    $validite_input_args['readonly'] = 1;
                $periode_validite = "<div class='flexrow'>";
                $validite_input_args['label'] = __("Période de validité : début");
                $periode_validite .= get_input_jpost($this, "date_debut", $validite_input_args);
                $validite_input_args['label'] = __("fin");
                $periode_validite .= get_input_jpost($this, "date_fin", $validite_input_args)."</div>";
            }
            
            $html .= "<td id='nom-{$this->id}'>".$icones.get_icone_aide("doc_".$this->type).$this->link_name.$entite_nom.$brouillon.$complet.$infos_doc.$periode_validite;
            if ($role == "admin")
                $html .= "<p>".decbin($this->valid)." / {$this->valid}</p>";
            $html .= "</td>"; 
        }
        
        // Les deux premiers boutons ne sont affichés que si le doc n'est pas un scan
        // Bouton brouillon
        if ($cols & Document::COL_DRAFT)
        {
            $html .= "<td>";
            if (!($this->valid & Document::SCAN))
            {
                $texte_bouton = (empty($this->last_modif)) ? __("Créer") : __("Mettre à jour");
                $html .= "<span class='doc-creer doc-bouton' data-ckw='{$this->ckw}'>$texte_bouton</span>";
            }
            $html .= "</td>";
        }
        
        // Bouton finaliser
        if ($cols & Document::COL_FINAL)
        {
            $html .= "<td>";
            if (!empty($this->last_modif)
                && !($this->valid & Document::SCAN)
                && !(($this->valid & Document::VALID_RESPONSABLE_NEED) XOR $this->signataire)
                && !($this->valid & (Document::VALID_RESPONSABLE_DONE | Document::VALID_CLIENT_DONE))
                )
            {
                $texte_bouton = ($this->valid & Document::DRAFT) ? __("Créer") : __("Mettre à jour");

                $html .= "<span class='doc-creer doc-bouton' data-final='valider'";
                if ($this->signataire)
                    $html .= " data-signataire='1'";
                $html .= ">$texte_bouton</span>";
            }
            $html .= "</td>";
        }
        
        // Bouton demander la signature
        //if (!$this->signataire && $cols & Document::COL_REQUEST)
        if ($cols & Document::COL_REQUEST)
        {
            $html .= "<td>";
            if (!empty($this->last_modif) && $this->valid & Document::VALID_RESPONSABLE_NEED)
            {
                if (!($this->valid & Document::VALID_RESPONSABLE_DONE))
                {
                    $etat_class = ($this->valid & Document::VALID_RESPONSABLE_REQUEST) ? "en-cours" : "";
                    $html .= "<span class='doc-demande-valid $etat_class doc-bouton' title='{$wpof->doc_demande_valid}'>".__("Demander")."</span>";
                }
            }
            $html .= "</td>";
        }
        
        // Bouton diffuser
        if ($cols & Document::COL_DIFFUSER)
        {
            $html .= "<td>";
            if (!empty($this->last_modif) && !($this->valid & Document::DRAFT))
            {
                $etat_class = ($this->visible) ? "fait" : "";
                $html .= "<span class='doc-diffuser $etat_class doc-bouton' title='{$wpof->doc_diffuser}'>".__("Diffuser")."</span>";
            }
            $html .= "</td>";
        }
        
        // bouton uploader
        if ($cols & Document::COL_SCAN)
        {
            $html .= '<td>';
            if (!empty($this->last_modif))
            {
                $id_span_filename = "file".rand();
                $id_span_message = "msg".rand();
                $html .= "<span class='doc-scan doc-bouton' title='{$wpof->doc_scan}'>".__("Déposer")."</span>";
                $html .= '<div class="dialog dialog-scan" style="display: none;">';
                $html .= '<form method="POST" name="upload-'.$this->id.'" enctype="multipart/form-data">';
                $html .= '<input name="scan-'.$this->id.'" type="file" accept="image/*,.pdf" />';
                $html .= '<p>';
                if ($this->valid & Document::VALID_RESPONSABLE_NEED)
                    $html .= '<label><input type="checkbox" name="signature_responsable" '.checked(is_signataire() != false, true, false).' /> '.__("signé par le⋅la responsable").'</label><br />';
                if ($this->valid & Document::VALID_CLIENT_NEED)
                    $html .= '<label><input type="checkbox" name="signature_client" /> '.__("signé par le⋅la client⋅e").'</label>';
                if ($this->valid & Document::VALID_STAGIAIRE_NEED)
                    $html .= '<label><input type="checkbox" name="signature_stagiaire" /> '.__("signé par le⋅la⋅les stagiaire(s)").'</label>';
                $html .= '</p>';
                $html .= hidden_input('id_span_message', $id_span_message);
                $html .= hidden_input('id_span_filename', $id_span_filename);
                $html .= hidden_input('action', 'archive_file');
                $html .= hidden_input('session_id', $this->session_formation_id);
                $html .= hidden_input('cols', $cols);
                $html .= hidden_input('signataire', ($this->signataire) ? 1 : 0);
                $html .= hidden_input('ligne_id', "tr-{$this->id}");
                $html .= '</form>';
                $html .= '<p id="'.$id_span_message.'" class="message"></p>';
                $html .= '</div>';
                $html .= '<span id="'.$id_span_filename.'" class="filename"></span>';
            }
            $html .= '</td>';
        }
        
        // Bouton supprimer
        if ($cols & Document::COL_SUPPRIMER)
        {
            $html .= "<td>"; 
            if (!empty($this->last_modif))
            {
                $html .= "<span class='doc-supprimer attention doc-bouton' title='{$wpof->doc_supprimer}'>".__("Supprimer")."</span>";
                $html .= "<div style='display: none' id='supprim{$this->id}'><p>{$wpof->doc_supprimer}</p></div>";
            }
            $html .= "</td>";
        }
        
        // Bouton rejeter
        if ($cols & Document::COL_REJETER)
        {
            $html .= "<td>"; 
            if ($this->valid & Document::VALID_RESPONSABLE_REQUEST)
                $html .= "<span class='doc-rejeter attention doc-bouton' title=''>".__("Rejeter")."</span>";
            $html .= "</td>";
        }
        
        $html .= "</tr>";
        
        return $html;
    }
    
    
    /*
     * Supprimer un document dans la base
     * Si $file vaut true, alors on supprime aussi les fichiers
     */
    public function supprimer($file = false)
    {
        if ($file)
        {
            unlink("{$this->path}/{$this->pdf_filename}");
            unlink("{$this->path}/{$this->html_filename}");
        }
        
        // dans tous les cas, on supprime le fichier .html.orig qui montre une modification manuelle du texte
        if (file_exists("{$this->path}/{$this->html_filename}.orig"))
            unlink("{$this->path}/{$this->html_filename}.orig");
        
        // mise à jour de la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("DELETE FROM $table
            WHERE session_id = '%d'
            AND contexte_id = '%d'
            AND contexte = '%d'
            AND document = '%s';",
            $this->session_formation_id, $this->contexte_id, $this->contexte, $this->type
        );
        
        return $wpdb->query($query);
    }
    
    public function pdf_creer($html = "")
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $options = new Options();
        $options->set('chroot', WP_CONTENT_DIR);
        $options->set('tempDir', WP_CONTENT_DIR."/uploads");
        $options->set('isRemoteEnabled', TRUE);
        $options->set('isPhpEnabled', TRUE);
        $options->set('defaultPaperSize', 'A4');
        $options->set('defaultPaperOrientation', $this->modele->orientation);
        $options->set('defaultFont', $wpof->pdf_texte_font);
        $options->set('dpi', 200);
        
        $this->dompdf = new Dompdf($options);
        
        if ($html == "")
        {
            // création du doc en HTML
            switch ($this->type)
            {
                case "emargement":
                    $session_formation->init_stagiaires();
                    $html = "";
                    if ($session_formation->type_emargement['jour'] == 1)
                        $html .= $this->get_html_emargement_journee();
                    if ($session_formation->type_emargement["tous"] == 1)
                        $html .= $this->get_html_emargement_journee("tous");
                    if ($session_formation->type_emargement["vide"] == 1)
                        $html .= $this->get_html_emargement_journee("vide");
                    if ($session_formation->type_emargement["stagiaire"] == 1)
                        $html .= $this->get_html_emargement_stagiaire();
                    break;
                case "eval_formation":
                    $html = $this->get_html_eval_formation();
                    break;
                case "quiz_connaissances":
                    $html = $this->get_html_quiz_connaissances();
                    break;
                case "pv_secu":
                    $html = $this->get_html_pv_secu();
                    break;
                case "proposition":
                    $html = $this->get_html_proposition();
                    break;
                default:
                    $html = $this->get_html_base();
                    break;
            }
        }
        elseif (!file_exists("{$this->path}/{$this->html_filename}.orig"))
            copy("{$this->path}/{$this->html_filename}", "{$this->path}/{$this->html_filename}.orig");
        
        if ($html != "")
        {
            
            $html = $this->html_from_model() . $html;
            $html = $this->flex_to_table($html);
            $this->html_save($html);
            
            //$html = $this->img_url_to_path($html); // l'utilisation d'image PNG locale semble poser problème (image vide intégrée dans le PDF)
            
            // output the HTML content
            $this->dompdf->loadHtml($html);
            $this->pdf_save();
            
            return true;
        }
        else
            return false;
    }
    
    /*
     * Création du document en HTML
     * Import du modèle
     * Substitution des variables
     */
    private function get_html_base()
    {
        if (isset($this->modele))
        {
            $html = "";
            if (preg_match('/<span [^>]*keyword[^>]*>([a-z]+:emargement_collectif.+)<\/span>/', $this->modele->contenu, $matches))
                $html .= $this->do_emargement_collectif($matches[1]);
            else
                $html .= $this->modele->contenu;
            return $this->substitution_auto(wpautop($html), "content");
        }
        else
            return "";
    }
    
    private function do_emargement_collectif($match)
    {
        $params = explode(':', $match);
        console_log($match);
        
        $nb_jour_max_par_page = (isset($params[2]) && $params[2] > 0) ? (integer) $params[2] : 1;
        
        $entite = $params[0];
        switch ($entite)
        {
            case "session":
                $session = $entite = get_session_by_id($this->session_formation_id);
                break;
            case "client":
                $entite = get_client_by_id($this->session_formation_id, $this->client_id);
                $session = get_session_by_id($this->session_formation_id);
                break;
            default:
                return wpautop($this->modele->contenu);
                break;
        }
        $fonction = 'get_doc_'.$params[1];
        
        // recalcul du nombre de jours maxi par page pour rééquilibrer sans dépasser la demande initiale
        $nb_jour_max = count($session->creneaux);
        $nb_page = (integer) ceil($nb_jour_max / $nb_jour_max_par_page);
        if ($nb_page == 0) return;
        $nb_jour_max_par_page = (integer) ceil($nb_jour_max / $nb_page);
        
        // calcul du nombre de stagiaires par page et du nombre de pages par plage de jours
        $nb_sous_page = 1; // nombre de sous-pages en fonction du nombre de stagiaires confirmés, si ça dépasse sur une seule
        if (substr_count($fonction, "vierge") == 0)
        {
            global $wpof;
            $nb_formateur = count($session->formateur);
            $nb_confirmes = $entite->nb_confirmes;
            $nb_sous_page = (integer) ceil(($nb_formateur + $nb_confirmes) / $wpof->nb_ligne_emargement);
        }

        $html = array();
        for($page = 0; $page < $nb_page; $page++)
        {
            $premier_jour_de_la_page = $page * $nb_jour_max_par_page;
            
            for ($sp = 0; $sp < $nb_sous_page; $sp++)
            {
                $tableau = $entite->$fonction('valeur', $nb_jour_max_par_page, $premier_jour_de_la_page, $sp);
                $dates_page = $entite->get_doc_emargement_dates_par_page('valeur', $nb_jour_max_par_page, $premier_jour_de_la_page, ($nb_sous_page > 1) ? $sp+1 ."/$nb_sous_page" : null);
                
                $index_page = "$page.$sp";
                
                $html[$index_page] = $this->modele->contenu;
                $html[$index_page] = str_replace('<span class="keyword">'.$match.'</span>', $tableau, $html[$index_page]);
                $html[$index_page] = str_replace('<span class="keyword">'.$entite->get_doc_emargement_dates_par_page('tag').'</span>', $this->brouillon_tag($dates_page), $html[$index_page]);
                
                $html[$index_page] = '<div class="emargement">'.$html[$index_page].'</div>';
            }
        }
        
        return join($this->get_saut_de_page(), $html);
    }

    /*
     * Renvoie un tableau d'une ligne avec les informations identifiant la session et le stagiaire
     */
    private function get_html_cartouche()
    {
        $stagiaire = get_stagiaire_by_id($this->session_formation_id, $this->contexte_id);
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $html = "<table class='cartouche'><tr>";
        $html .= "<td>{$session_formation->titre_formation}</td>";
        $html .= "<td>{$session_formation->dates_texte}</td>";
        $html .= "<td>{$session_formation->ville}</td>";
        $html .= "<td>".$stagiaire->get_displayname()."</td>";
        $html .= "</tr></table>";
        
        return $html;
    }
    
    private function get_html_proposition()
    {
        global $wpof;
        
        if ($this->contexte & $wpof->doc_context->formation)
        {
            $entite = get_formation_by_id($this->contexte_id);
            $titre = $entite->titre;
        }
        else
        {
            $entite = get_session_by_id($this->session_formation_id);
            $titre = $entite->titre_formation;
        }
        
        ob_start();
        ?>
        <h1><?php echo $titre; ?></h1>
        <?php if ($this->contexte & $wpof->doc_context->formation) : ?>
        <p><?php printf(__("Programme générique en date du %s. Il peut être adapté à vos besoins."), $entite->date_modif); ?></p>
        <?php else : ?>
        <p><?php echo $wpof->documents->get_term('proposition'); ?>. <?php printf(__("Document en date du %s."), $entite->date_modif); ?></p>
        <?php endif; ?>
        <table class="proposition">
        <?php
        $desc_proposition = $wpof->desc_formation->get_group("proposition");
        foreach($desc_proposition->term as $k => $term)
        {
            if ($k == "programme")
                continue; // on traite le programme plus loin
                
            if (!empty($entite->$k))
                echo "<tr><td>{$term->text}</td><td>".wpautop($entite->$k)."</td></tr>";
            elseif ($this->valid & Document::DRAFT)
                printf("<tr><td>{$term->text}</td><td>".$this->brouillon_tag(__("Vous n'avez pas défini « %s ». Ce message n'apparait que dans le brouillon."))."</td></tr>", $term->text);
        }
        ?>
        
        <?php if ($this->contexte & $wpof->doc_context->formation) : ?>
        <tr><td><?php _e("Tarif indicatif"); ?></td>
        <td>
            <p>
            <?php
                if ($entite->duree > 0)
                {
                    echo "<strong>".get_tarif_formation($entite->tarif * $entite->duree)."</strong>";
                    echo "<br />".__("soit")." ";
                }
                echo get_tarif_formation($entite->tarif)." ".__("de l'heure");
            echo $wpof->of_exotva ? "<br />".$wpof->terms_exo_tva : "";
            ?>
            </p>
            <p><?php echo $wpof->terms_tarif_inter.". ".$wpof->terms_tarif_groupe; ?></p>
        </td>
        </tr>
        <tr><td><?php _e("Durée"); ?></td>
        <td>
        <p><?php printf(__("%d heures"), $entite->duree); ?>
        <?php if ($entite->nb_jour != "") echo $entite->nb_jour; ?>
        </p>
        </td>
        </tr>
        <?php else : ?>
            <?php if (!is_user_logged_in()) : ?>
            <tr><td><?php _e("Tarif par stagiaire"); ?></td>
            <td>
            <p><strong><?php echo get_tarif_formation($entite->tarif_total_chiffre); ?></strong> <?php printf(__("soit %s de l'heure"), get_tarif_formation($entite->tarif_heure)); ?> <?php if ($wpof->of_exotva) echo "({$wpof->terms_exo_tva})"; ?></p>
            </td>
            </tr>
            <?php endif; ?>
        <tr><td><?php _e("Dates et durée"); ?></td>
        <td>
        <p><?php echo $entite->dates_texte; ?></p>
        <p><?php echo ($entite->nb_heure != $entite->nb_heure_estime) ? __("Durée estimée") : __("Durée"); echo " ".$entite->nb_heure_estime; ?></p>
        </td>
        </tr>
        <tr><td><?php _e('Planning de la session'); ?></td><td><?php echo $entite->get_decoupage_temporel(); ?></td></tr>
        <?php endif; ?>
        
        <tr>
        <td><?php
            $pluriel = (count($entite->formateur) > 1) ? "s" : "";
            echo __("Intervenant(e)").$pluriel;
            ?>
        </td>
        <td>
        <?php echo join($this->get_liste_formateurs($entite, true)); ?>
        </td>
        </tr>
        
        </table>

        <h3><?php _e("Programme"); ?></h3>
        <div class="proposition">
        <?php echo wpautop($entite->programme); ?>
        </div>
        <?php 
        return ob_get_clean();
    }
    
    private function get_liste_formateurs($entite, $presentation = false)
    {
        $liste_formateurs = array();
        $presentation_formateurs = array();
        foreach($entite->formateur as $f)
        {
            $display_name = get_displayname($f);
            $liste_formateurs[] = $display_name;
        
            if ($presentation)
            {
                $photo_id = get_user_meta($f, 'photo', true);
                $img_tag = "";
                if ($photo_id)
                {
                    $src = str_replace('https', 'http', wp_get_attachment_url($photo_id));
                    $img_tag = "<img class='photo-formateur' src='".$src."' />";
                }
                
                $cv_tag = ($this->valid & Document::DRAFT) ? "<p>".$this->brouillon_tag(__("CV manquant, vous devez en fournir un !"))."</p>" : "";
                $src = get_user_meta($f, 'cv_url', true);
                if (empty($src))
                {
                    $cv_id = get_user_meta($f, 'cv', true);
                    if (!empty($cv_id))
                        $src = wp_get_attachment_url($cv_id);
                }
                
                if (!empty($src))
                    $cv_tag = "<p>Son CV : <a href='$src'>".$src."</a></p>";
                    
                $realisations = wpautop(get_user_meta($f, "realisations", true));
                if (!empty($realisations))
                    $realisations = "<p><strong>".__("Ses réalisations")."</strong></p>".$realisations;
                else
                    $realisations = "";
                
                $presentation_formateurs[] = "<div class='fiche-formateur'>$img_tag<h4>".$display_name."</h4>".wpautop(get_user_meta($f, 'presentation', true)).$cv_tag.$realisations."</div>";
            }
        }
        
        if ($presentation)
            return $presentation_formateurs;
        else
            return $liste_formateurs;
    }
    
    public function get_dialog_keywords()
    {
        global $tinymce_wpof_settings;
        ob_start();
        
        if ($this->ckw > 0) : ?>
            <div class="custom_keyword_dialog">
            <form>
            <input type="hidden" name="action" value="doc_save_custom_keywords" />
            <input type="hidden" name="contexte" value="<?php echo $this->contexte; ?>" />
            <input type="hidden" name="contexte_id" value="<?php echo $this->contexte_id; ?>" />
            <input type="hidden" name="session_id" value="<?php echo $this->session_formation_id; ?>" />
            <input type="hidden" name="type_doc" value="<?php echo $this->type; ?>" />
            
            <p><?php _e("Les champs à remplir obligatoirement sont marqués par"); ?> <span class="important dashicons dashicons-edit"></span>
            
            <?php foreach($this->modele->ckw as $key => $val) :
                if (!isset($this->$key)) $this->$key = "";
            ?>
            <div class="ckw_bloc">
                <p class="ckw_desc openButton <?php echo ($val->needed) ? "open" : "close"; ?>" data-id="div_<?php echo $key; ?>">
                    <?php echo $val->desc; ?> 
                    <?php if ($val->needed) : ?><span class="important dashicons dashicons-edit"></span>
                    <?php endif; ?>
                </p>
                    <?php if (!empty($val->defaut)) : ?>
                    <span class="float right icone-bouton add_keyword_defaut" data-id="ckw_<?php echo $key; ?>" data-type="<?php echo $val->type; ?>" data-content="<?php echo $val->defaut; ?>"><?php _e("Valeur par défaut"); ?></span>
                    <?php endif; ?>
                <div id="div_<?php echo $key; ?>" class="<?php echo ($val->needed) ? "" : "blocHidden" ; ?>">
                <?php
                switch ($val->type)
                {
                    case "bool":
                        ?>
                        <input type="radio" name="<?php echo $key; ?>" id="ckw_<?php echo $key."oui"; ?>" value="1" <?php checked(1, $this->$key, true); ?> /><label for="ckw_<?php echo $key."oui"; ?>" class="inline">Oui</label> | 
                        <input type="radio" name="<?php echo $key; ?>" id="ckw_<?php echo $key."non"; ?>" value="0" <?php checked(0, $this->$key, true); ?> /><label for="ckw_<?php echo $key."non"; ?>" class="inline">Non</label> 
                        <?php break;
                    case "text":
                        wp_editor($this->$key, $key, array_merge($tinymce_wpof_settings, array('textarea_rows' => 5, 'textarea_name' => $key)));
                        break;
                    case "number":
                        ?>
                        <input type="number" step="0.001" class="long-text" id="ckw_<?php echo $key; ?>" value="<?php echo $this->$key; ?>" name="<?php echo $key; ?>" />
                        <?php break;
                    default:
                        if (isset($this->val))
                            printf("Mauvais type de donnée : %s", $this->val);
                        else
                            printf("Pas de type de donnée pour %s", $key);
                        break;
                }
                ?>
                </div>
            </div>
            <?php endforeach; ?>
            </form>
            </div>
        <?php endif;
        
        return ob_get_clean();
    }
    
    private function get_html_eval_formation()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $corps = $wpof->eval_form;
        
        $html = "";
        $html .= $this->get_html_cartouche();
        
        $html .= "<table class='eval'>";
        
        foreach (preg_split("/\n/", $corps) as $line)
        {
            $line = explode("|", $line);
            
            $line_type = trim($line[0]);
            if (in_array($line_type, array("", "#"))) continue;
            
            switch ($line_type)
            {
                case "h":
                    $html .= "<tr><td class='td-titre'>".trim($line[1])."</td><td class='td-plusmoins'>–</td><td></td><td></td><td></td><td class='td-plusmoins'>+</td></tr>";
                    break;
                case "r":
                    $html .= "<tr><td class='td-question'>".trim($line[1])."</td>";
                    $i = 1;
                    while ($i <= 5)
                    {
                        $html .= "<td class='td-value'>$i</td>";
                        $i++;
                    }
                    $html .= "</tr>";
                    break;
                case "t":
                case "ta":
                    $html .= "<tr><td class='td-question'>".trim($line[1])."</td><td colspan='5'></td></tr>";
                    $html .= "<tr><td class='td-reponse-$line_type' colspan='6'></td></tr>";
                    break;
            }
        }
        
        $html .= "</table>";
        
        return $html;
    }

    private function get_html_quiz_connaissances()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        $quiz = new Quiz($session_formation->quizobj_id);
        $quiz->init_questions($session_formation->quizobj_id);
        
        $html = "";
        $html .= $this->get_html_cartouche();
        
        
        $html .= "<table class='eval'>";
        
        foreach($quiz->questions as $titre => $groupe)
        {
            if ($titre == "none") $titre = "";
            
            $html .= "<tr><td class='td-titre'><h3>$titre</h3></td><td class='td-plusmoins'>-</td><td> </td><td> </td><td> </td><td class='td-plusmoins'>+</td></tr>\n";

            foreach($groupe as $num => $q)
            {
                $html .= "<tr><td class='quiz-question td-question'>$q</td>";
                $i = 1;
                while ($i <= 5)
                {
                    $html .= "<td class='td-value'>$i</td>";
                    $i++;
                }
                $html .= "</tr>\n";
            }
        }
        $html .= "</table>";
        
        return $html;
    }


    private function get_html_pv_secu()
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        
        $html = "";
        
        if (!empty($session_formation->lieu_secu_erp))
        {
            $html .= "<h1>".__("PV de commission de sécurité")."</h1>";
            $html .= wp_get_attachment_image($session_formation->lieu_secu_erp, 'fullsize', false, array('class' => 'pv-secu', 'align' => 'center'));
            return $html;
        }
        else
            return "";
    }
    
    /*
    * Création de la feuille d'émargement par jour
    * $content vaut
    * * "vide" pour créer des feuilles vierges de noms
    * * "tous" pour mentionner tous les présents, pas seulement ceux marqués comme émargeant
    */
    private function get_html_emargement_journee($content = "")
    {
        global $wpof;
        global $SessionStagiaire;
        
        $session_formation = get_session_by_id($this->session_formation_id);

        $emargement = get_post($wpof->emargement_id);
        $modele_page = "<div class='emargement'>".wpautop($emargement->post_content)."</div>";
        
        $contenu = "";
        foreach($session_formation->dates_array as $d)
        {
            $nb_stagiaires = 0;
            $colonnes = array();
            $liste_stagiaires = array();
            
            $contenu .= $modele_page;
            $contenu = str_replace("{titre}", $d, $contenu);
            
            foreach($session_formation->creneaux[$d] as $c)
            {
                if (!in_array($c->type, array("foad_sync", "foad_async")))
                    $colonnes[] = "<th class='plage'>".$c->titre."<br />".$wpof->type_creneau[$c->type]."</th>";
            }
            if ($wpof->doc_emarge_recu_ri != "")
                $colonnes['ri'] = "<th>".$wpof->doc_emarge_recu_ri."</th>";
            
            $tableau_complet = "<table class='tableau-stagiaires'><tbody><tr><th class='client'>".__("Client")."</th><th class='stagiaire'>".__("Stagiaire")."</th>".join($colonnes)."</tr>";
            
            foreach($session_formation->clients as $client_id)
            {
                $client = get_client_by_id($this->session_formation_id, $client_id);
                foreach($client->stagiaires as $stagiaire_id)
                {
                    $session_stagiaire = get_stagiaire_by_id($this->session_formation_id, $stagiaire_id);
                    if (in_array($d, $session_stagiaire->dates_array) && ($session_stagiaire->confirme || $content == "tous"))
                    {
                        $tableau_complet .= "<tr><td>";
                        if ($content != "vide")
                        {
                            if ($client->financement != "part" && isset($client->nom))
                                $tableau_complet .= $client->nom;
                        }
                        $tableau_complet .= "</td><td>";
                        if ($content != "vide")
                            $tableau_complet .= $session_stagiaire->get_displayname();
                        $tableau_complet .= "</td>";
                            
                        foreach($session_formation->creneaux[$d] as $c)
                            if (!in_array($c->type, array("foad_sync", "foad_async")))
                            {
                                if ($session_stagiaire->creneaux[$c->id] == 1 || $content == "vide" || $content == "tous")
                                    $tableau_complet .= "<td></td>";
                                else
                                    $tableau_complet .= "<td class='stagiaire-absent'></td>";
                            }
                        
                        if (isset($colonnes['ri']))
                            $tableau_complet .= "<td></td>";
                        $tableau_complet .= "</tr>";
                        $nb_stagiaires ++;
                    }
                }
            }
            
            $i = $nb_stagiaires;
            while ($i <= $session_formation->stagiaires_max)
            {
                $tableau_complet .= "<tr>".str_repeat("<td></td>", count($colonnes) + 2)."</tr>"; // 2 : colonne client + colonne stagiaire
                $i++;
            }
            
            $tableau_complet .= "</tbody></table>";
            
            $contenu = str_replace("{tableau-stagiaires}", $tableau_complet, $contenu);
            $contenu .= "<div class='saut-de-page'></div>";
        }
        
        return $this->substitute_values(wpautop($contenu));
    }
    
    /*
    * Création de la feuille d'émargement par stagiaire
    */
    private function get_html_emargement_stagiaire()
    {
        global $wpof;
        global $SessionStagiaire;
        
        $session_formation = get_session_by_id($this->session_formation_id);

        $emargement = get_post($wpof->emargement_id);
        $modele_page = "<div class='emargement'>".wpautop($emargement->post_content)."</div>";
        
        if ($wpof->doc_emarge_recu_ri != "")
            $ligne_valid_ri = "<tr><td colspan='2'>".$wpof->doc_emarge_recu_ri."</td><td> </td></tr>";
        
        $contenu = "";
        foreach($session_formation->inscrits as $stagiaire_id)
        {
            $session_stagiaire = $SessionStagiaire[$stagiaire_id];
            
            if ($session_stagiaire->confirme)
            {
                $contenu .= $modele_page;
                $stagiaire = $session_stagiaire->get_displayname();
                if ($session_formation->type_index == "inter" && $session_stagiaire->entreprise != "")
                    $stagiaire .= " (".$session_stagiaire->entreprise.")";
                $contenu = str_replace("{titre}", $stagiaire, $contenu);
                
                $tableau_complet = "<table class='tableau-dates'><tbody>";
                $tableau_complet .= "<tr><th>".__("Date")."</th><th>".__("Créneau")." / ".__("Type")."</th><th>".__("Signature")."</th></tr>";
                
                $last_date = "";
                foreach($session_formation->creneaux as $date => $creneaux)
                {
                    foreach($creneaux as $c)
                    {
                        if ($session_stagiaire->creneaux[$c->id] == 1 && !in_array($c->type, array("foad_sync", "foad_async")))
                        {
                            $cell = "td";
                            if ($date != $last_date)
                            {
                                $last_date = $date;
                                $cell = "th";
                            }
                            $tableau_complet .= "<tr><$cell>".pretty_print_dates($c->date)."</$cell><td>".$c->titre." / ".$wpof->type_creneau[$c->type]."</td><td> </td></tr>";
                        }
                    }
                }
                
                $tableau_complet .= $ligne_valid_ri."</tbody></table>";
                $contenu = str_replace("{tableau-stagiaires}", $tableau_complet, $contenu);
                $contenu .= "<div class='saut-de-page'></div>";
            }
        }
        
        return $this->substitute_values(wpautop($contenu));
    }


    /*
     * Enregistrement d'un fichier HTML avec le contenu du document sans html_model
     * Servira à générer des documents PDF regroupant plusieurs documents
     */
    private function html_save($html)
    {
        if (!is_dir($this->path)) mkdir($this->path, 0777, true);
        
        $this->html_filename = $this->base_filename.".html";
        
        $html_file = fopen($this->path."/".$this->html_filename, "w");
        fwrite($html_file, $html);
    }

    /*
     * Création du document PDF avec son nom et son chemin
     * retourne le nom du fichier PDF
     */
    private function pdf_save()
    {
        global $wpof;
        if (!is_dir($this->path)) mkdir($this->path, 0777, true);
        
        $this->pdf_filename = $this->base_filename.".pdf";
        $this->dompdf->render();
        
        if (is_user_logged_in())
        {
            $pdf_content = $this->dompdf->output();
            
            $pdf_name = $this->path."/".$this->pdf_filename;
            $pdf_file = fopen($pdf_name, "w");
            fwrite($pdf_file, $pdf_content);
            fclose($pdf_file);
        }
        else
        {
            // si le créateur du document n'est pas connecté on est sur de la création de proposition de formation ou de session
            if ($this->contexte & $wpof->doc_context->formation)
            {
                $formation = get_formation_by_id($this->contexte_id);
                $filename = "formation-".$formation->slug;
            }
            else
            {
                $session = get_session_by_id($this->contexte_id);
                $filename = "session-".$session->slug;
            }
            
            $this->dompdf->stream($filename);
        }
    }
    
    /*
    * Modèle de contenu HTML header et footer (modèle de base)
    * Comme on ne peut invoquer plusieurs fois loadHtml, on prépare une chaîne html envoyée en une seule fois
    */
    private function html_from_model()
    {
        global $wpof;

/*        $html .= "h1.saut-page {page-break-before: always; break-before: always;}"; // force le saut de page avant les h1 de classe saut-page
        $html .= "h1.saut-page:nth-of-type(1) { page-break-before: avoid; break-before: avoid; }"; // sauf le premier*/

        ob_start();
        ?>
<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="utf-8">
<style type='text/css'>
@page
{
    margin: <?php echo $wpof->pdf_marge_haut; ?>mm <?php echo $wpof->pdf_marge_droite; ?>mm <?php echo $wpof->pdf_marge_bas; ?>mm <?php echo $wpof->pdf_marge_gauche; ?>mm;
}
#header { height: <?php echo $wpof->pdf_hauteur_header; ?>mm; }
#footer { height: <?php echo $wpof->pdf_hauteur_footer; ?>mm; bottom: 0mm; }
body
{
    margin: <?php echo $wpof->pdf_hauteur_header; ?>mm 0 <?php echo $wpof->pdf_hauteur_footer; ?>mm 0;
}
<?php echo file_get_contents(wpof_path . "/css/pdf.css"); ?>

h1 { color: <?php echo $wpof->pdf_couleur_titre_doc; ?>; }
h2, h3, h4, h5, h6 { color: <?php echo $wpof->pdf_couleur_titre_autres; ?>; }
h1, h2, h3, h4, h5, h6 { font-family: <?php echo $wpof->pdf_titre_font; ?>; }

<?php echo $wpof->pdf_css; ?>
</style>
</head>
<body>
<div id='header'><?php echo wpautop($this->substitution_auto($wpof->pdf_header, "header", false)); ?></div>
<div id='footer'><?php echo wpautop($this->substitution_auto($wpof->pdf_footer, "footer", false)); ?>
    <div class='page-number'></div>
</div>
    <?php
        return ob_get_clean();
    }

    /*
    * Modèle de contenu HTML avec le footer mis dans le header
    * Comme on ne peut invoquer plusieurs fois loadHtml, on prépare une chaîne html envoyée en une seule fois
    */
    private function html_from_model_header_only()
    {
        global $wpof;

        // calcul totalement empirique : le header de ce modèle a une hauteur égale à la somme de celle du header et celle du footer définies dans les options
        $hauteur_header = $wpof->pdf_hauteur_header + $wpof->pdf_hauteur_footer;

        $html = "";
        
        $html .= "<style type='text/css'>";
        $html .= "@page { margin: ".$wpof->pdf_marge_haut."mm ".$wpof->pdf_marge_droite."mm ".$wpof->pdf_marge_bas."mm ".$wpof->pdf_marge_gauche."mm; }";
        $html .= "h1.saut-page {page-break-before: always; break-before: always;}"; // force le saut de page avant les h1 de classe saut-page
        $html .= "h1.saut-page:nth-of-type(1) { page-break-before: avoid; break-before: avoid; }"; // sauf le premier
        $html .= "#header { height: ".$hauteur_header."mm; width: 50%; }";
        $html .= "body { margin: ".$hauteur_header."mm 0 0 0; }";
        $html .= $wpof->pdf_css;
        $html .= "#footer { height: ".$hauteur_header."mm; top: 0mm; width: 100%; border: none; }";
        $html .= "#footer p { text-align: right; font-size: 0.9em; line-height: 1.1em; padding-left: 120mm; color: cmyk(0, 0, 0, 1); }";
        $html .= "</style>";
        $html .= "<div id='header'>".wpautop($wpof->pdf_header)."</div>";
        $html .= "<div id='footer'>".wpautop($wpof->pdf_footer)."</div>";
        
        return $html;
    }

    /*
     * Convertit les URL des images en chemin absolu
     * Évite la copie des images dans un dossier temporaire avant de les inclure dans le PDF
     */
    private function img_url_to_path($html)
    {
        /*
        $dom = new DOMDocument("1.0","UTF-8");
        $dom->loadHTML(utf8_decode($html));

        foreach ($dom->getElementsByTagName('img') as $img)
        {
            $src = $img->getAttribute("src");
            $src = str_replace(WP_CONTENT_URL, WP_CONTENT_DIR, $src);
            $img->setAttribute("src", $src);
        }
        return $dom->saveHTML();
        */
        return preg_replace('#(<img)(.*)(src=["\'])'.WP_CONTENT_URL.'([^"\']*)(["\'][^>]*>)#', '$1$2$3'.WP_CONTENT_DIR.'$4$5', $html);
    }
    
    private function get_saut_de_page()
    {
        return "<div class='saut-de-page'></div>";
    }
    
    private function brouillon_tag($content, $force = false)
    {
        if ($force || ($this->valid & Document::DRAFT))
            return '<span class="brouillon">'.$content.'</span>';
        else
            return $content;
    }
    
    private function flex_to_table($html)
    {
        if (preg_match('<div class="flex">', $html) == 1)
        {
            $doc = new DOMDocument("1.0", "utf-8");
            $doc->loadHTML($html);
            foreach($doc->getElementsByTagName("div") as $node)
            {
                $class = explode(" ", $node->getAttribute('class'));
                if (in_array('flex', $class))
                {
                    $table = $doc->createElement("table");
                    $table->setAttribute('class', join(" ", $class));
                    $tr = $doc->createElement("tr");
                    
                    foreach($node->childNodes as $n)
                    {
                        if (trim($n->textContent) != "" || $n->hasChildNodes())
                        {
                            $td = $doc->createElement('td');
                            $td->appendChild($n->cloneNode(true));
                            $tr->appendChild($td);
                        }
                    }
                    $table->appendChild($tr);
                    
                    $node->parentNode->replaceChild($table, $node);
                }
            }
            $html = utf8_decode($doc->saveHTML());
        }
        
        return $html;
    }
    
    /*
    * Substitution automatique des variables disséminées dans les modèles de document
    * selon le schéma : <span class="keyword">entité:variable</span>
    * entité peut être :
    * * of (l'organisme de formation, options préfixées de wpof_of_)
    * * formation (la fiche de formation)
    * * session (la session de formation)
    * * client
    * * stagiaire
    * * formateur (l'équipe pédagogique de la formation ou de la session)
    *
    * À part of toutes ces entités sont dépendantes du contexte du document
    */
    private function substitution_auto($text, $part, $highlight = false)
    {
        global $wpof;
        global $document_actuel;
        $document_actuel = $this;
        
        if ($part == "content")
            $document_actuel->infos_manquantes = array();
        
        if ($highlight === false)
            $highlight = $this->valid & Document::DRAFT;
        
        $document_html = HtmlDomParser::str_get_html($text);
        
        /*$matches = array();
        preg_match_all('/\{[a-z]+:[^}]+\}/', $text, $matches, PREG_PATTERN_ORDER);
        */
        
        // Analyse des mots-clés
        foreach($document_html->findMulti('.keyword') as $var)
        {
            if ($var)
            {
                $non_reconnu = false;
                
                $vars = explode(':', $var->innertext);
                $entite_type = $vars[0];
                $param_nom = $vars[1];
                $entite = array();
                $get_doc_func = "get_doc_$param_nom";
                
                $param = null;
                if (isset($vars[2]))
                    $param = $vars[2];
                    
                $valeur = array();
                
                switch($entite_type)
                {
                    case 'of':
                        $entite[0] = $wpof;
                        break;
                    //case 'formation':
                    case 'session':
                        $func = "get_".$entite_type."_by_id";
                        $entite[0] = $func($this->session_formation_id);
                        //return var_export($entite, true);
                        break;
                    case 'client':
                        $entite[0] = get_client_by_id($this->session_formation_id, $this->client_id);
                        break;
                    case 'stagiaire':
                        $entite[0] = get_stagiaire_by_id($this->session_formation_id, $this->stagiaire_id);
                        break;
                    case 'formateur':
                        $entite = get_tab_formateur_by_tab_id($this->array_formateur_id);
                        break;
                    case 'document':
                        $entite[0] = $this;
                        break;
                    default:
                        $non_reconnu = true;
                        break;
                }
                if (!empty($entite) && method_exists($entite[0], $get_doc_func))
                {
                    foreach($entite as $e)
                    {
                        if ($param)
                            $valeur[] = trim($e->$get_doc_func('valeur', $param));
                        else
                            $valeur[] = trim($e->$get_doc_func('valeur'));
                    }
                    $desc = $entite[0]->$get_doc_func('desc');
                }
                elseif ($entite_type == "document")
                {
                    $desc = $entite[0]->modele->ckw[$param_nom]->desc;
                    if (isset($entite[0]->$param_nom))
                        $valeur[] = trim($entite[0]->$param_nom);
                    else if ($entite[0]->modele->ckw[$param_nom]->needed && $highlight)
                        $valeur = '<span class="doc_info_absente" data-func="'.$param_nom.'" data-entite="'.$entite_type.'" data-entitenom="'.$this->titre.'" data-desc="'.$desc.'">'.$desc.'</span>';
                }
                else
                    $non_reconnu = true;
                    
                if ($non_reconnu)
                    $var->outertext = $this->brouillon_tag($var->innertext." non reconnu", true);
                else
                {
                    if (is_array($valeur))
                        $valeur = join(', ', $valeur);
                    if (empty($valeur) && $highlight)
                    {
                        $entite_nom = (method_exists($entite[0], 'get_displayname')) ? $entite[0]->get_displayname() : "";
                        $valeur = '<span class="doc_info_absente" data-func="'.$get_doc_func.'" data-entite="'.$entite_type.'" data-entitenom="'.$entite_nom.'" data-desc="'.$desc.'">'.$desc.'</span>';
                    }
                    $var->outertext = $this->brouillon_tag($valeur, $highlight);
                }
            }
        }
        
        // Analyse des tests
        foreach($document_html->findMulti('.test') as $var)
        {
            if ($var)
            {
                $non_reconnu = false;
                
                $vars = explode('|', $var->innertext);
                $condition = trim($vars[0]);
                if (substr_count($condition, "doc_info_absente") > 0)
                    $condition = "";
                else
                    $condition = strip_tags($condition);
                
                if (count($vars) > 1)
                {
                    $sioui = (empty($vars[1])) ? "" : $vars[1];
                    $sinon = (empty($vars[2])) ? "" : $vars[2];
                    
                    $test_type = $test_sign = "";
                    $op = array();
                    
                    if (!empty($condition))
                        foreach($wpof->test_modele->get_fields("sign") as $key => $sign)
                        {
                            $html_sign = htmlentities($sign);
                            if (substr_count($condition, $html_sign) > 0)
                            {
                                $test_type = $key;
                                $test_sign = $sign;
                                $op = array_map('sanitize_title', explode($html_sign, $condition));
                                break;
                            }
                        }
                        if ($test_type == "")
                        {
                            if (!empty($condition))
                            {
                                $test_type = "vrai";
                                $result = $sioui;
                            }
                            else
                            {
                                $test_type = "faux";
                                $result = $sinon;
                            }
                        }
                        else
                        {
                            eval('$test_result = ($op[0] '.$test_sign.' $op[1]);');
                            $result = ($test_result) ? $sioui : $sinon;
                        }
                        
                        $var->outertext = $result;
                }
                else
                {
                    $non_reconnu = true;
                    $var->outertext = "<span class=\"brouillon\">Test non reconnu : ".$var->innertext."</span>";
                }
            }
        }
        
        // Comptage des infos manquantes
        if ($part == "content")
        {
            foreach($document_html->findMulti('.doc_info_absente') as $var)
                $this->infos_manquantes[] = array
                (
                    'doc_func' => $var->getAttribute("data-func"),
                    'entite_type' => $var->getAttribute("data-entite"),
                    'entite_nom' => $var->getAttribute("data-entitenom"),
                    'desc' => $var->getAttribute("data-desc"),
                );
            $this->update_meta("infos_manquantes");
        }
        
        return $document_html->save();
    }
    
    public function get_doc_titre($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "document:$function_name", 'desc' => __("Intitulé du document"));
        return $result[$arg];
    }
    public function get_doc_date_debut($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "document:$function_name", 'desc' => __("Date de début de validité"));
        return $result[$arg];
    }
    public function get_doc_date_fin($arg = 'valeur')
    {
        $function_name = str_replace('get_doc_', '', __FUNCTION__);
        $result = array('valeur' => $this->$function_name, 'tag' => "document:$function_name", 'desc' => __("Date de fin de validité"));
        return $result[$arg];
    }
}

/*
* Crée un tableau pour gérer les documents administratifs
*/
function get_gestion_docs($objet)
{
    global $Documents;
    global $wpof;
    
    // l'utilisateur courant peut-il signer les documents
    $signataire = is_signataire();
    
    // quelle est la source de documents ?
    $doc_necessaire = $objet->doc_necessaire;
    $doc_uid_suffix = "-{$objet->doc_suffix}";
    
    // si aucun document n'est nécessaire (j'en doute), on ne fait rien
    if (count($doc_necessaire) == 0) return "";
            
    $html = "";
    
    ob_start();
    ?>
    <table class='gestion-docs-admin'><tbody>
    <tr class="tr-titre">
        <th><?php _e("Document"); echo " ".get_icone_aide("document"); ?></th>
        <th><?php _e("Brouillon"); echo " ".get_icone_aide("brouillon"); ?></th>
        <th><?php _e("Final"); echo " ".get_icone_aide("final"); ?></th>
        <?php if (!$signataire): ?>
            <th><?php _e("Signature responsable"); echo " ".get_icone_aide("signature_responsable"); ?></th>
        <?php endif; ?>
        <th><?php _e("Diffuser"); echo " ".get_icone_aide("diffuser"); ?></th>
        <th><?php _e("Scan"); echo " ".get_icone_aide("scan"); ?></th>
        <th><?php _e("Supprimer"); echo " ".get_icone_aide("supprimer"); ?></th>
    </tr>
    <?php
    
    $html .= ob_get_clean();
    
    // note : $doc->id = $doc_uid = $doc_id . $doc_uid_suffix
    foreach($doc_necessaire as $doc_id)
    {
        $doc =& $Documents[$doc_id.$doc_uid_suffix];
        $doc->signataire = $signataire;
        
        $html .= $doc->get_html_ligne();
    }
    
    $html .= "</tbody></table>";
    
    return $html;
}

add_action('wp_ajax_get_html_doc_content', 'get_html_doc_content');
function get_html_doc_content()
{
    $reponse = $_POST;
    
    $document = new Document($_POST['typedoc'], $_POST['session_id'], $_POST['contexte'], $_POST['contexte_id']);
    ob_start();
    ?>
    <div class='dialog'><?php wp_editor(file_get_contents("{$document->path}/{$document->html_filename}"), "content".time()); ?></div>;
    <?php
    $reponse['html'] = ob_get_clean();
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_update_html_doc_content', 'update_html_doc_content');
function update_html_doc_content()
{
    $reponse = $_POST;
    
    $document = new Document($_POST['typedoc'], $_POST['session_id'], $_POST['contexte'], $_POST['contexte_id']);
    $document->pdf_creer($_POST['content']);
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_doc_check_custom_keywords', 'doc_check_custom_keywords');
function doc_check_custom_keywords()
{
    $reponse = array();
    $contexte_id = $_POST['contexte_id'];
    $contexte = $_POST['contexte'];
    $session_id = $_POST['session_id'];
    $type_doc = $_POST['type_doc'];
    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
    
    $empty_kw = array();
    
    foreach($doc->modele->ckw as $key => $val)
    {
        if (empty($doc->$key))
            $empty_kw[$key] = $val;
    }
    
    $reponse['ckw'] = count($empty_kw);
    $reponse['dialog'] = $doc->get_dialog_keywords();
    
    echo json_encode($reponse);
    die();
}

add_action('wp_ajax_doc_save_custom_keywords', 'doc_save_custom_keywords');
function doc_save_custom_keywords()
{
    $contexte_id = $_POST['contexte_id'];
    $contexte = $_POST['contexte'];
    $session_id = $_POST['session_id'];
    $type_doc = $_POST['type_doc'];
    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
    $manque = 0;
    
    foreach(array_keys($doc->modele->ckw) as $key)
    {
        if (isset($_POST[$key]))
            $doc->update_meta($key, $_POST[$key]);
        else
            $manque++;
    }
    $doc->set_ckw_ok();
    
    $reponse['manque'] = $manque;
    $reponse['lignedoc'] = $doc->get_html_ligne();
    
    echo json_encode($reponse);
    die();
}

/*
 * Vérification de la présence et de la complétion de mots-clés personnalisés
 */


/*
 * Fonction de traitement d'un document
 * Utilisée via une requête POST Ajax
 */
function traitement_doc()
{
    global $SessionFormation;
    global $wpof;
    
    $contexte_id = $_POST['contexte_id'];
    $contexte = $_POST['contexte'];
    $session_id = $_POST['session_id'];
    $type_doc = $_POST['type_doc'];
    $action_doc = $_POST['action_doc'];
    
    ob_start(); // permet de récupérer les éventuels message d'erreur ou alerte dans $reponse['log'] et éviter de foirer l'analyse JSON de $reponse
    
    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);

    $reponse = array('message' => '', 'lignedoc' => '');
    
    switch($action_doc)
    {
        case "final":
        case "brouillon":
            if ($action_doc == "brouillon")
                $doc->valid |= Document::DRAFT;
            else
                $doc->valid &= ~Document::DRAFT;
            
            $doc->valid &= ~(Document::VALID_ALL_DONE);
            
            $doc->update_meta('valid', $doc->valid);
            
            if ($_POST["signataire"] == 1)
                $doc->signataire = true;
            
            if ($doc->pdf_creer())
            {
                $doc->update_meta('pdf_filename', $doc->pdf_filename);
                $doc->update_meta('html_filename', $doc->html_filename);
                $doc->update_meta('last_modif', current_time('timestamp'));
                
                $doc->init_link_name();
            }
            else
                $reponse['message'] .= "<span class='erreur'>".__("Création échouée, le document est vide !")."</span>";
            // TODO : faire un objet JSON avec champs html, erreur, demande_valid, diffuser
            break;
            
        case "demande_valid":
            $doc->update_meta('valid', $doc->valid ^ Document::VALID_RESPONSABLE_REQUEST);
            if ($doc->valid & Document::VALID_RESPONSABLE_REQUEST)
            {
                $session = get_session_by_id($session_id);
                $subject = "[Formacoop] Document à signer pour ".$session->get_formateurs_noms();
                $content_message = "Bonjour,\n\nNouveau document à valider :\n".$doc->titre." pour ".$session->get_formateurs_noms()."\n";
                $content_message .= "\nPour la session :\n".$session->titre_formation."\n\ndébutant le ".$session->first_date."\n";
                $content_message .= "\nConnectez-vous pour le lire → ".home_url()."/login";
                $message = new Message(array('to' => get_signataires_id(), 'content' => $content_message, 'subject' => $subject));
                $message->sendmail();
                $reponse['message'] = "Notification faite à ".implode(', ', $message->to_email);
            }
            break;
            
        case "diffuser":
            $doc->update_meta('visible', !$doc->visible);
            // TODO : envoi un message au stagiaire
            break;
        case "supprimer":
            $doc->supprimer();
            $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
            // TODO : éventuellement supprimer le fichier
            break;
        case "rejeter":
            $doc->update_meta('valid', $doc->valid & ~ Document::VALID_RESPONSABLE_REQUEST);
            $session = get_session_by_id($session_id);
            $resp = get_formateur_by_id(get_current_user_id());
            $subject = "[Formacoop] Refus de validation pour ".$doc->titre;
            $content_message = "Bonjour,\n\nVotre document :\n".$doc->titre." a été refusé par ".$resp->get_displayname()."\n";
            $content_message .= "\nPour la session :\n".$session->titre_formation."\n\ndébutant le ".$session->first_date."\n";
            if (!empty($doc->infos_manquantes))
            {
                $content_message .= "\nDe manière automatique OPAGA a détecté que ces informations sont manquantes :\n";
                foreach($doc->infos_manquantes as $key => $val)
                    $content_message.= "  — $val\n";
            }
            $content_message .= "\nConnectez-vous sur la page de la session pour corriger →\n".$session->permalien;
            $message = new Message(array('to' => get_signataires_id(), 'content' => $content_message, 'subject' => $subject));
            $message->sendmail();
            $reponse['message'] = "Notification faite à ".implode(', ', $message->to_email);
            break;
    }
    $reponse['lignedoc'] = $doc->get_html_ligne((isset($_POST['cols'])) ? $_POST['cols'] : Document::COL_ALL);

    $reponse['log'] = ob_get_clean();

    echo json_encode($reponse);

    die();
}
add_action('wp_ajax_traitement_doc', 'traitement_doc');

function doc_get_infos_manquantes()
{
    $contexte_id = $_POST['contexte_id'];
    $contexte = $_POST['contexte'];
    $session_id = $_POST['session_id'];
    $type_doc = $_POST['type_doc'];
    $reponse = array('erreur' => '');
    
    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
    
    if (!empty($doc->infos_manquantes))
        $reponse['erreur'] = "<h3>".__("Informations manquantes")."</h3>";
    foreach($doc->infos_manquantes as $info)
        $reponse['erreur'] .= "<p><em>".str_replace("get_doc_", "", $info['doc_func'])."</em> : <strong>".$info['desc']."</strong> pour ".$info['entite_type']." <strong>".$info['entite_nom']."</strong></p>";
    
    $reponse['erreur'] = '<div class="left">'.$reponse['erreur'].'</div>';
    echo json_encode($reponse);
    die();
}
add_action('wp_ajax_doc_get_infos_manquantes', 'doc_get_infos_manquantes');

/*
 * Fonction de traitement d'un document
 * Utilisée via une requête POST Ajax
 * TODO : fonction inutilisée actuellement. Elle permettait de créer une compilation de docuemnts en un seul en récupérant le HTML
 * Peut-être qu'elle pourrait servir à autre chose
 */
function export_pdf()
{
    global $SessionFormation;
    global $SessionStagiaire;
    global $Documents;
    
    $user_id = (isset($_POST['user_id'])) ? $_POST['user_id'] : -1;
    $doc_id = (isset($_POST['doc_id'])) ? $_POST['doc_id'] : "";
    $session_id = $_POST['session_id'];
    
    $suffixe = ($doc_id != "") ? "-$doc_id" : "";
    
    $session_formation = $SessionFormation[$session_id] = new SessionFormation($session_id);
    $session_formation->init_stagiaires();
    
    if ($user_id > 0)
    {
        $SessionStagiaire[$user_id]->init_docs();
        $suffixe .= "-".$SessionStagiaire[$user_id]->user->user_nicename;
    }
    else
    {
        $session_formation->init_docs();
        foreach($session_formation->inscrits as $i)
            $SessionStagiaire[$i]->init_docs();
    }
    
    $first_doc = reset($Documents);
    $html = $first_doc->html_from_model();
    
    foreach($Documents as $doc)
    {
        if ($doc_id == "" || $doc_id == $doc->type)
        {
            $file_content = file_get_contents($doc->path."/".$doc->html_filename);
            
            if ($file_content != "")
            {
                $html .= $file_content;
                $html .= "<div class='saut-de-page'></div>";
            }
        }
    }
    
    $options = new Options();
    $options->set('chroot', WP_CONTENT_DIR);
    $options->set('tempDir', WP_CONTENT_DIR."/uploads");
    $options->set('isRemoteEnabled', TRUE);
    $options->set('isPhpEnabled', TRUE);
    $options->set('defaultPaperSize', 'A4');
    $options->set('dpi', 200);
    
    $dompdf = new Dompdf($options);
    $dompdf->loadHtml($html);
    $dompdf->render();
    $pdf_content = $dompdf->output();
    
    $pdf_name = sanitize_title($session_formation->titre_session.$suffixe).".pdf";
    $pdf_file = fopen($first_doc->path."/".$pdf_name, "w");
    fwrite($pdf_file, $pdf_content);
    fclose($pdf_file);
    
    echo "<a href='{$first_doc->url}/$pdf_name'>$pdf_name</a>";
    
    die();
}
add_action( 'wp_ajax_export_pdf', 'export_pdf' );


add_action('wp_ajax_archive_file', 'archive_file');
function archive_file()
{
    require_once(wpof_path . "/class/class-upload.php");
    
    $reponse = array('message' => '', 'filename' => '', 'lignedoc' => '');
    
    if (isset($_POST['session_id']) && $_POST['session_id'] > 0)
    {
        $session_id = $_POST['session_id'];
        global $wpof;
        $session_formation = get_session_by_id($session_id);
        
        if (!empty($_POST['contexte_id']) && $_POST['contexte_id'] > 0)
        {
            $contexte_id = $_POST['contexte_id'];
            $contexte = $_POST['contexte'];
        }
        else
        {
            $contexte_id = -1;
            $contexte = $wpof->doc_context->session;
        }
        
        for ($i = 0; $i < $_POST['nb_files']; $i++)
        {
            if ($_FILES["files"]["error"][$i] > 0)
            {
                $reponse['message'] .= "<span class='erreur'>".__("Erreur de téléversement code ")." ".$_FILES["files"]["error"][$i]." ".__("pour le fichier")." ".$_FILES["files"]["name"][$i]."</span>";
            }
            else
            {
                $upload = new Upload($session_id);
                
                if (!is_dir($upload->path))
                    mkdir($upload->path, 0777, true);
                
                if (isset($_POST['type_doc']))
                {
                    $type_doc = $_POST['type_doc'];
                
                    $doc = new Document($type_doc, $session_id, $contexte, $contexte_id);
                    
                    $extension = explode(".", $_FILES['files']['name'][0]);
                    $extension = end($extension);

                    $upload->filename = $doc->pdf_filename = preg_replace('/pdf$/', $extension, $doc->pdf_filename);
                    
                    $doc->update_meta('html_filename', "");
                    $doc->update_meta('last_modif', current_time('timestamp'));
                    $doc->valid |= Document::SCAN;
                    $doc->valid &= ~Document::DRAFT;
                    
                    if ($_POST["signature_responsable"] == "true")
                    {
                        $doc->valid |= Document::VALID_RESPONSABLE_DONE;
                        $doc->valid &= ~ Document::VALID_RESPONSABLE_REQUEST;
                        if (is_signataire() !== false) // dans ce cas, on est dans le tableau de bord responsable et donc on fait disparaître le bouton Rejeter et on envoie un mail aux contacts de la session
                        {
                            $reponse['remove'] = "tr#tr-{$doc->id} .doc-rejeter";
                            $subject = "[Formacoop] Document signé : ".$doc->titre." pour la session ".$session_formation->titre_session;
                            $content_message = "Bonjour,\n\nLe document :\n".$doc->titre." a été signé\n";
                            $content_message .= "\nPour la session :\n".$session_formation->titre_formation."\n\ndébutant le ".$session_formation->first_date."\n";
                            $content_message .= "\nTu peux le télécharger (connexion requise)\n → ".$doc->href."\n";
                            $content_message .= "\nou le diffuser à ton client depuis la page de session\n → ".$session_formation->permalien."\n";
                            $content_message .= "\nCe message est envoyé automatiquement par l'équipe de ".$wpof->of_nom." ce qui n'empêche pas de souhaiter une bonne journée !";
                            $message = new Message(array('to' => $session_formation->formateur, 'content' => $content_message, 'subject' => $subject));
                            if ($message->sendmail() == false)
                                $reponse['message'] .= '<span class="erreur">'.__("L'email n'a pas été envoyé à ").$session_formation->get_formateurs_noms().'</span><br />';
                            else
                                $reponse['message'] .= '<span class="succes">'.__("Email envoyé à ").$session_formation->get_formateurs_noms().'</span><br />';
                        }
                    }
                    else
                        $doc->valid &= ~ Document::VALID_RESPONSABLE_DONE;
                    
                    if ($_POST["signature_client"] == "true")
                        $doc->valid |= Document::VALID_CLIENT_DONE;
                    else
                        $doc->valid &= ~ Document::VALID_CLIENT_DONE;
                        
                    if ($_POST["signature_stagiaire"] == "true")
                        $doc->valid |= Document::VALID_STAGIAIRE_DONE;
                    else
                        $doc->valid &= ~ Document::VALID_STAGIAIRE_DONE;
                    
                    $doc->update_meta('valid', $doc->valid);
                    
                    $doc->init_link_name();
                    
                    $reponse['lignedoc'] = $doc->get_html_ligne((isset($_POST['cols'])) ? $_POST['cols'] : Document::COL_ALL);
                }
                else
                    $upload->filename = $_FILES['files']['name'][$i];
                
                if (file_exists($upload->path . $upload->filename))
                    rename($upload->path . $upload->filename, $upload->path . preg_replace("((.*)\.)", "$1-".date("Ymd_His").".", $upload->filename));
                move_uploaded_file( $_FILES['files']['tmp_name'][$i], $upload->path . $upload->filename);
                
                if (!isset($_POST['type_doc']))
                {
                    $upload->set_md5sum();
                    
                    $upload->timestamp = time();
                    $upload->date_text = date_i18n("j F Y H:i:s", $upload->timestamp);
                    
                    $session_formation->uploads[$upload->md5sum] = $upload;
                }
                
                $reponse['message'] .= "<span class='succes'>".__("Fichier correctement copié")." ".$upload->filename."</span>";
            }
        }
        update_post_meta($session_id, "uploads", serialize($session_formation->uploads));
        foreach ($session_formation->uploads as $u)
        {
            $reponse['filename'] .= $u->get_html('tr');
        }
    }
    else
    {
        $reponse['message'] .= "<span class='erreur'>".__("Aucune session à laquelle rattacher ce fichier")."</span>";
    }
    
    $log = $_POST;
    unset($log['files']);
    $reponse['log'] = $log;
    
    echo json_encode($reponse);

    die();
}

add_action('wp_ajax_delete_scan_file', 'delete_scan_file');
function delete_scan_file()
{
    require_once(wpof_path . "/class/class-upload.php");

    $reponse = array('message' => '', 'succes' => '');
    
    if (isset($_POST['session_id']) && $_POST['session_id'] > 0)
    {
        $session_id = $_POST['session_id'];
        $md5sum = $_POST['md5sum'];
        
        global $SessionFormation;
        $SessionFormation[$session_id] = new SessionFormation($session_id);
        $session_formation =& $SessionFormation[$session_id];
        
        $upload = $session_formation->uploads[$md5sum];
        
        if (file_exists($upload->path . $upload->filename))
        {
            unlink($upload->path . $upload->filename);
            $reponse['message'] = "<span class='succes'>".sprintf(__("Supression effective de %s"), $upload->path . $upload->filename)."</span>";
        }
        else
            $reponse['message'] = "<span class='erreur'>".sprintf(__("Le fichier %s n'existe pas"), $upload->path . $upload->filename)."</span>";
            
        $reponse['succes'] = true;
        unset($session_formation->uploads[$md5sum]);
        update_post_meta($session_id, "uploads", serialize($session_formation->uploads));
    }
        
    echo json_encode($reponse);

    die();
}
