<?php
/*
 * wpof-admin.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

 
/**
 * Add post type lieu
 */
function register_cpt_lieu() {

	/**
	 * Post Type: Lieux.
	 */

	$labels = array(
		"name" => __( "Lieux", "generic" ),
		"singular_name" => __( "Lieu", "generic" ),
		"all_items" => __( "Tous les lieux", "generic" ),
		"add_new" => __( "Ajouter un nouveau", "generic" ),
		"add_new_item" => __("Ajouter un nouveau lieu"),
		"view_item" => __("Voir le lieu"),
		"edit_item" => __("Modifier le lieu"),
		"update_item" => __("Mettre à jour le lieu"),
	);

	$args = array(
		"label" => __( "Lieux", "generic" ),
		"labels" => $labels,
		"description" => "Fiche descriptive du lieu",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"delete_with_user" => false,
		"show_in_rest" => false,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => array( "slug" => "lieu", "with_front" => true ),
		"query_var" => true,
		"menu_position" => 6,
		"menu_icon" => "dashicons-location-alt",
		"supports" => array( "title", "thumbnail" ),
		"taxonomies" => array(),
	);

	register_post_type( "lieu", $args );
}

add_action( 'init', 'register_cpt_lieu' );


// add meta box
add_action('add_meta_boxes','initialisation_lieu_metaboxes');
function initialisation_lieu_metaboxes()
{
    wp_enqueue_media();
    add_meta_box('lieu-data', __('Caractéristiques du lieu'), 'lieu_data_meta_box', 'lieu', 'normal', 'high');
}

function lieu_data_meta_box($post)
{
    $adresse = $code_postal = $ville = $localisation = $secu_erp = $secu_erp_name = "";
    $lieu = get_post_meta($post->ID);
    if ($lieu)
    {
        $adresse = $lieu['adresse'][0];
        $code_postal = $lieu['code_postal'][0];
        $ville = $lieu['ville'][0];
        $localisation = $lieu['localisation'][0];
        $secu_erp = $lieu['secu_erp'][0];
        $secu_erp_name = ($secu_erp != "") ? get_the_title($secu_erp) : "";
    }
    ?>
    
    <p>
    <label for="adresse"><?php _e("Adresse"); ?> </label>
    <input name="adresse" type="text" value="<?php echo $adresse; ?>" />
    </p>
    <p>
    <label for="code_postal"><?php _e("Code postal"); ?> </label>
    <input name="code_postal" type="text" value="<?php echo $code_postal; ?>" />
    </p>
    <p>
    <label for="ville"><?php _e("Ville"); ?> </label>
    <input name="ville" type="text" value="<?php echo $ville; ?>" />
    (<?php _e("Indiquez au moins cette information si vous n'avez pas tous les détails pour le lieu"); ?>)
    </p>
    <p>
    <label for="localisation"><?php _e("Localisation"); ?></label><br />
    <em><?php _e("Insertion de code pour embarquer une carte avec pointeur (Openstreetmap par exemple)"); ?></em><br />
    <textarea name="localisation" rows="5" cols="60"><?php echo $localisation; ?></textarea>
    
    </p>
    <p>
    <label for="secu_erp"><?php _e("PV de commission de sécurité ERP"); ?></label><br />
    <input type="hidden" id="secu_erp" name="secu_erp" value="<?php echo $secu_erp; ?>" />
    <a href="#" class="clickButton button-add-media" data-valueid="secu_erp" data-linkmedia="erp-nom-image" id="erp-add-image"><?php _e("Téléversez une image"); ?></a>
    <a id="erp-nom-image" target="_blank" href="<?php echo get_attachment_link($secu_erp); ?>"><?php echo $secu_erp_name; ?></a>
    
    </p>
    
    <?php
}        

// save meta box with update
add_action('save_post','save_lieu_metaboxes');
function save_lieu_metaboxes($post_ID)
{
    if (get_post_type($post_ID) != "lieu") return;
    
    $champs = array
    (
        'ville',
        'code_postal',
        'adresse',
        'localisation',
        'secu_erp',
    );
    foreach($champs as $c)
    {
        if(isset($_POST[$c]))
            update_post_meta($post_ID, $c, $_POST[$c]);
    }
}
