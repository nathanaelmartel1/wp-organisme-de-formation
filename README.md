# OPAGA, pour gérer votre organisme de formation (en France)

OPAGA est un logiciel libre développé depuis novembre 2017 par Dimitri Robert et qui a pour objectif de gérer tous ces aspects administratifs.

L’idée, aujourd’hui, est de proposer une interface simple qui permette l’accompagnement des formateurs et responsables de formation pour satisfaire aux obligations légales que sont le bilan pédagogique et financier et les audits et contrôles dans le cadre de Datadock et Qualiopi. Ce logiciel s’adresse aux organismes concourant au développement des compétences (nouveau nom pour organisme de formation).

## En savoir plus

OPAGA s'occupe de la partie administrative de la formation professionnelle. En revanche, il ne gère pas la comptabilité (pas de création de devis ni de factures) et ne propose pas de plateforme pédagogique (*LMS* ou *learning managment system*).

Visitez le site dédié : <https://opaga.fr> et notamment la [FAQ](https://opaga.fr/page/faq.html) pour trouver les réponses à vos questions. Si ce n'est pas le cas, vous pouvez toujours [poser vos questions à l'auteur](https://opaga.fr/page/contact.html).

OPAGA est diffusé sous licence [Affero GPL v3](https://www.gnu.org/licenses/why-affero-gpl.fr.html). Cela signifie que vous pouvez le télécharger, l'installer sur votre serveur, le modifier et partager vos modifications sous la même licence.

En revanche, cela ne veut pas dire qu'OPAGA est gratuit ! Le développement a un coût non négligeable et je vous invite à lire la page dédiée au [financement](https://opaga.fr/page/financement.html). De plus, au pied de chaque page vous trouverez l'état d'avancement du financement.

## Documentation utilisateur

La documentation en cours de rédaction est lisible ici : <https://doc.opaga.fr>

## Installation

> **Attention**
>
> Utilisez la branche rc !
>
> La branche master est périmée et n'est conservée que pour fusion prochaine.
> 
> La branche test est la branche de développement en cours et testée chez Coopaname.

OPAGA est une extension pour WordPress. Vous devez donc disposer d'un serveur Web avec tout ce qui est nécessaire au fonctionnement de WordPress (PHP, MySQL, la base quoi). deux possibilités :

* téléchargez OPAGA depuis framagit sous la forme d'une archive ZIP et déposez-la dans votre WordPress depuis la page des extensions ;
* clonez OPAGA avec git depuis votre hébergement.

Vous aurez également besoin de l'extension [Ultimate Member](https://fr.wordpress.org/plugins/ultimate-member/), utilisée pour une gestion plus fine des comptes utilisateurs et des rôles (cette dépendance est vouée à disparaître).

OPAGA utilise [dompdf](https://github.com/dompdf/dompdf) pour la création des documents PDF. Pas besoin de l'installer en sus, il est inclus. Là aussi assurez-vous que les dépendances sont satisfaites.

> **Note**
>
> Le dépôt framagit ainsi que les noms de fichiers font référence au nom `wp-organisme-de-formation` ou `WPOF` ou `wpof` qui était le nom provisoire d'OPAGA. Un jour, je changerai cela dans le code, mais ce n'est pas urgent.
