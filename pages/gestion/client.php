<?php
/*
 * pages/gestion/client.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with wpof program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

function get_page_gestion_client()
{
    global $wpof;
    $html = '<h2 class="gestion">'.$wpof->pages_gestion['client'].'</h2>';
    
    $html .= get_tableau_gestion_clients();
    
    return $html;
}

function get_tableau_gestion_clients()
{
    ob_start(); ?>
    <p>À venir bientôt !</p>
    <table class="opaga opaga2 edit-data">
    <thead>
    </thead>
    </table>
    <?php
    return ob_get_clean();
}

?>
